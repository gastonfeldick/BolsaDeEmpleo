$(document).ready(function(){


    function tabla(){


        $.ajax({
            url: '..controladores/filtros.php',
            type: 'post',
            data: "algo",
            success: function(respuesta){
                console.log(respuesta);
                let tarea= JSON.parse(respuesta);
                let template="";
                tarea.forEach(t =>{
                    template+=`

                    <tr>
                        <td>${t.id_alumno}</td>
                        <td>${t.nombre}</td>
                        <td>${t.nombreCarrera}</td>
                        <td>${t.nombre_genero}</td>
                        <td>${t.year}<?= $year?></td>
                        <td>${t.nombre_estado_civil}</td>
                        <td>${t.nombre_localidad}</td>
                        <td>disponible</td>
                        <td>?</td>
                        <td>foto</td>
                        <td>
                            <a href="verUsuario.php?id=<?=$datos->id_alumno?>" class="btn btn-primary "  name="btn-ver"> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
                            <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
                            <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
                            </svg> </a>
                            <a href="eliminar.php?id=<?=$datos->id_alumno?>" class="btn btn-small btn-danger" name="btn-modificar"><i class="bi bi-pencil-fill"></i></a>
                            <a href="ver.php?id=<?=$datos->id_alumno?>" class="btn btn-small btn-info" name="btn-curriculum"> <i class="bi bi-file-text"></i></a>
                        /td>
                    </tr>

                    `;
                });
                $('#tabla').html(template);
            }
        });
    }

    $('#formulario').submit(function(e){
        let search="algo";



        $.ajax({
            url:'../controladores/filtrosBackend.php',
            type:'post',
            data: $('#formulario').serialize(),
            //data: $('#formulario').serialize(),
            success: function(respuesta){
                let tarea=JSON.parse(respuesta);
                console.log(tarea);
                let template="";
                tarea.forEach(t=>{
                    template+=`
                    <tr>
                        <td> ${t.id_alumno} </td>
                        <td> ${t.nombre} </td>
                        <td> ${t.nombre_carrera} </td>
                        <td> ${t.nombre_genero}</td>
                        <td> ${t.year} </td>
                        <td> ${t.nombre_estado_civil} </td>
                        <td> ${t.nombre_localidad} </td>
                        <td>disponible</td>
                        <td>?</td>
                        <td>foto</td>
                        <td>
                            <a href="verUsuario.php?id=${t.id_alumno}" class="btn btn-primary "  name="btn-ver"> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
                            <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
                            <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
                            </svg> </a>
                            <a href="eliminar.php?id=${t.id_alumno}>" class="btn btn-small btn-danger" name="btn-modificar"><i class="bi bi-pencil-fill"></i></a>
                            <a href="ver.php?id=${t.id_alumno}" class="btn btn-small btn-info" name="btn-curriculum"> <i class="bi bi-file-text"></i></a>
                        </td>
                    </tr>

                    `;
                });

                $('#tabla').html(template);


            }
        });

        e.preventDefault();
    });

});
