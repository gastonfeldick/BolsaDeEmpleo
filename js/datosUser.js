$(document).ready(function(){
    rellenarDatos();
    tablaIdiomas();
    contacto();
    carrera();
    secundaria();
    Universidad();
    conocimientoInformatico();
    perfilProfesional();

    $('#formulario').show();
    $('#cancelar').click(function(e){
        /*if(!confirm("¿Esta seguro de cancelar? Volvera al menu ")){
            e.preventDefault();
        }*/
    });

    $('#formulario').submit(function(e){
        console.log("formulario");
        $.ajax({
            url: '../controladores/backend/insertarAlumnos.php',
            type: 'post',
            data: $('#formulario').serialize(),
            success: function(respuesta){
                console.log(respuesta);
                let variable=JSON.parse(respuesta);
                console.log(variable);
                $('#formularioContacto').show();
            }
        });
        e.preventDefault();
    });

    $('#formularioContacto').submit(function(e){
        $.ajax({
            url: '../controladores/backend/insertarContacto.php',
            type: 'post',
            data: $('#formularioContacto').serialize(),
            success: function(respuesta){
                let variable=JSON.parse(respuesta);
                console.log(variable);
            }
        });

        e.preventDefault();
    });

    $('#formularioCarrera').submit(function(e){
        $.ajax({
            url: '../controladores/backend/insertarCarrera.php',
            type: 'post',
            data: $('#formularioCarrera').serialize(),
            success: function(respuesta){
                console.log(respuesta);
                carrera();
            }
        });

        e.preventDefault();
    });

    $('#formularioIdiomas').submit(function(e){
        $.ajax({
            url: '../controladores/backend/insertarIdioma.php',
            type: 'post',
            data: $('#formularioIdiomas').serialize(),
            success: function(respuesta){
                console.log(respuesta);
                tablaIdiomas();
                //let variable=JSON.parse(respuesta);
                //console.log(variable);
            }
        });

        e.preventDefault();
    });

    $('#formularioAcademico').submit(function(e){



        e.preventDefault();
    });





    //curriculum
    $('#formCurriculum').submit(e =>{
        e.preventDefault()
        if (controlTypePdf()!=false){

            var parametros = new FormData($('#formCurriculum')[0]);
            $.ajax ({
                data : parametros,
                url : 'Funcionalidades/EnvioCurriculum.php',
                type : 'POST',
                contentType : false,
                processData : false,
                beforesend : function(){
                },
                succes: function(response){
                    alert(response);
                },
            });
        }
    });

    $('#formularioAcademicoSecundaria').submit(function(e){
        $.ajax({
            url: '../controladores/backend/insertarSecundaria.php',
            type: 'post',
            data: $('#formularioAcademicoSecundaria').serialize(),
            success: function(respuesta){
                console.log(respuesta);

            }
        });
        e.preventDefault();
    });

    $('#formularioAcademicoUniversitario').submit(function(e){
        $.ajax({
            url: '../controladores/backend/insertarUniversidad.php',
            type: 'post',
            data: $('#formularioAcademicoUniversitario').serialize(),
            success: function(respuesta){
                console.log(respuesta);
                Universidad();

            }
        });
        e.preventDefault();
    });

    $('#formularioConocimientoInformatico').submit(function(e){
        $.ajax({
            url: '../controladores/backend/insertarConocimiento.php',
            type: 'post',
            data: $('#formularioConocimientoInformatico').serialize(),
            success: function(respuesta){
                console.log(respuesta);
                conocimientoInformatico();

            }
        });
        e.preventDefault();
    });

    $('#formularioPerfilProfesional').submit(function(e){
        $.ajax({
            url: '../controladores/backend/insertarPerfil.php',
            type: 'post',
            data: $('#formularioPerfilProfesional').serialize(),
            success: function(respuesta){
                console.log(respuesta);
            }
        });
        e.preventDefault();
    });

    $('#formuluarioExperienciaLaboral').submit(function(e){
        $.ajax({
            url: '../controladores/backend/insertarExperiencia.php',
            type: 'post',
            data: $('#formuluarioExperienciaLaboral').serialize(),
            success: function(respuesta){
                console.log(respuesta);
            }
        });
        e.preventDefault();
    });



    function controlType (){//controla el tipo de archivo de la imagen
        var filePath = $('#archivo').val();
        var type = filePath.split('.')
        var extension = type.pop();
        //console.log(extension);
        var permitted = 'jpg';
        if (permitted!=extension){
            alert('Asegurate de que el archivo sea de tipo jpg');
            filepath = '';
            return false;
        }
        else{
            return extension;
        }
    }



    function rellenarDatos(){
        let id=$('#id').val();

        console.log(id);
        $.ajax ({
            url : '../controladores/backend/datosAlumno.php',
            type : 'post',
            data : {id},
            success: function(respuesta){
                let res=JSON.parse(respuesta);
                console.log(res);
                res.forEach(element => {
                    $('#nombre').val(element.nombre);
                    $('#apellido').val(element.apellido);
                    $('#dni').val(element.documento);
                    $('#fecha').val(element.fecha_nacimiento);
                    $('#genero').val(element.genero);
                    $('#estadoCivil').val(element.estado_civil);
                    $('#hijos').val(element.cantidad_hijos);
                    $('#nacionalidad').val(element.nacionalidad);
                    $('#discapacidad').val(element.discapacidad);
                    $('#localidad').val(element.localidad);
                    $('#provincias').val(element.id_provincia);
                    $('#pais').val(element.pais);
                    $('#codigoPostal').val(element.codigo_postal);
                    $('#grupoSanguineo').val(element.grupo_sanguineo);
                });

            }
        });
        console.log("funciona");
    }

    function contacto(){
        let id=$('#id').val();
        $.ajax ({
            url : '../controladores/backend/datosContacto.php',
            type : 'post',
            data : {id},
            success: function(respuesta){
                let res=JSON.parse(respuesta);
                console.log(res);
                res.forEach(element => {
                    $('#correo').val(element.correo);
                    $('#telefono').val(element.num_celular);
                    $('#telefonoFijo').val(element.num_fijo);
                    $('#facebook').val(element.facebook);
                    $('#linkedin').val(element.linkedin);
                });

            }
        });

    }


    function tablaIdiomas(){
        let id=$('#id').val();
        console.log(id);
        $.ajax({
            url : '../controladores/backend/tablaIdiomas.php',
            type : 'post',
            data : {id},
            success: function(respuesta){
                  console.log(respuesta);
                  let tarea= JSON.parse(respuesta);
                  let template="";

                  i=0;
                  tarea.forEach(t =>{
                      template+=`

                          <tr idIdioma=${t.id_usuario} idioma=${t.idioma} >
                              <td class="clo-4">${t.id_usuario}</td>
                              <td>${t.idioma}</td>

                              <td>
                                  ${t.nivel}
                              </td>
                              <td>
                                  <button class="eliminar btn btn-danger" id="eliminar">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/><path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/></svg>

                              </td>
                          </tr>


                      `;
                      console.log("nivel: ");
                      console.log(t.nivel);

                  });
                  $('#bodyIdiomas').html(template);



            }

        });
    }

    function carrera(){
        let id=$('#id').val();
        $.ajax({
            url : '../controladores/backend/datosCarrera.php',
            type : 'post',
            data : {id},
            success: function(respuesta){
                  console.log(respuesta);
                  let tarea= JSON.parse(respuesta);
                  let template="";
                  tarea.forEach(t =>{

                      template+=`

                          <tr idUsuario=${t.id_usuario} idCarrera=${t.id_carrera} >
                              <td>${t.id_carrera}</td>

                              <td>
                                  ${t.carrera}
                              </td>
                              <td>
                                  <button class="eliminarCarrera btn btn-danger" id="eliminar">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/><path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/></svg>

                              </td>
                          </tr>


                      `;

                  });
                  $('#bodyCarreras').html(template);
            }

        });

    }

    function secundaria(){
        let id=$('#id').val();
        $.ajax ({
            url : '../controladores/backend/datosSecundaria.php',
            type : 'post',
            data : {id},
            success: function(respuesta){
                let res=JSON.parse(respuesta);
                res.forEach(element => {
                    $('#instituto').val(element.instituto);
                    $('#titulo').val(element.titulo);
                    $('#desde').val(element.desde);
                    $('#hasta').val(element.hasta);
                });

            }
        });



    }

    function Universidad(){
        let id=$('#id').val();
        $.ajax ({
            url : '../controladores/backend/datosUniversidad.php',
            type : 'post',
            data : {id},
            success: function(respuesta){
                console.log(respuesta);
                let tarea=JSON.parse(respuesta);
                let template="";

                tarea.forEach(t =>{
                    template+=`
                            $universidad=$_POST["universidad"];
                            $titulo=$_POST["tituloUniversitario"];
                            $desde=$_POST["desdeUniversitario"];
                            $hasta=$_POST["hastaUniversitario"];
                        <tr id_usuario=${t.id_usuario} universidad=${t.universidad} titulo=${t.titulo}>
                            <td class="clo-4">${t.universidad}</td>
                            <td>${t.titulo}</td>
                            <td>${t.desde}</td>
                            <td>${t.hasta}</td>
                            <td>
                                <button class="eliminarUniversidad btn btn-danger" id="eliminar">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/><path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/></svg>

                            </td>
                        </tr>


                    `;


                });
                $('#bodyUniversitario').html(template);


            }
        });



    }

    function conocimientoInformatico(){

      let id=$('#id').val();
      $.ajax ({
          url : '../controladores/backend/datosConocimiento.php',
          type : 'post',
          data : {id},
          success: function(respuesta){
              console.log(respuesta);
              let tarea=JSON.parse(respuesta);
              let template="";

              tarea.forEach(t =>{
                  template+=`
                          $universidad=$_POST["universidad"];
                          $titulo=$_POST["tituloUniversitario"];
                          $desde=$_POST["desdeUniversitario"];
                          $hasta=$_POST["hastaUniversitario"];
                      <tr id_usuario=${t.id_usuario} programa=${t.programa}>
                          <td class="clo-4">${t.id_usuario}</td>
                          <td>${t.programa}</td>
                          <td>
                              <button class="eliminarPrograma btn btn-danger" id="eliminar">
                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16"><path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/><path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/></svg>

                          </td>
                      </tr>


                  `;


              });
              $('#bodyProgramas').html(template);


          }
      });
    }

    function perfilProfesional(){
        let id=$('#id').val();
        $.ajax ({
            url : '../controladores/backend/datosPerfil.php',
            type : 'post',
            data : {id},
            success: function(respuesta){
                let res=JSON.parse(respuesta);
                console.log(res);
                res.forEach(element => {
                    $('#cargo').val(element.cargo);
                    $('#carta').val(element.carta);
                });

            }
        });

    }

    $(document).on('click','.eliminar',function(e){
        let variable=$(this)[0].parentElement.parentElement;
        const idIdioma=$(variable).attr('idIdioma');
        const idioma=$(variable).attr('idioma');
        let arreglo={
            id: idIdioma,
            idioma: idioma,
        };

        if (confirm("¿Desea eliminar el idioma?")){
            $.post("../controladores/backend/eliminarIdioma.php",arreglo,function(e){
                console.log(e);
                tablaIdiomas();
            });
        }

        e.preventDefault();
    });

    $(document).on('click','.eliminarCarrera',function(e){
        let variable=$(this)[0].parentElement.parentElement;
        const idUsuario=$(variable).attr('idUsuario');
        const idCarrera=$(variable).attr('idCarrera');
        let arreglo={
            id: idUsuario,
            idCarrera: idCarrera,
        };

        if (confirm("¿Desea eliminar la carrera?")){
            $.post("../controladores/backend/eliminarCarrera.php",arreglo,function(e){
                console.log(e);
                carrera();
            });
        }

        e.preventDefault();
    });

    $(document).on('click','.eliminarUniversidad',function(e){
        let variable=$(this)[0].parentElement.parentElement;
        const id_usuario=$(variable).attr('id_usuario');
        const universidad=$(variable).attr('universidad');
        const titulo=$(variable).attr('titulo');
        let arreglo={
            id: id_usuario,
            universidad: universidad,
            titulo: titulo,
        };

        if (confirm("¿Desea eliminar la universidad?")){
            $.post("../controladores/backend/eliminarUniversidad.php",arreglo,function(e){
                console.log(e);

            });
            Universidad();
        }

        e.preventDefault();
    });

    $(document).on('click','.eliminarPrograma',function(e){
        let variable=$(this)[0].parentElement.parentElement;
        const id_usuario=$(variable).attr('id_usuario');
        const programa=$(variable).attr('programa');
        let arreglo={
            id: id_usuario,
            programa: programa,
        };

        if (confirm("¿Desea eliminar el programa?")){
            $.post("../controladores/backend/eliminarConocimiento.php",arreglo,function(e){
                console.log(e);
                conocimientoInformatico()

            });

        }

        e.preventDefault();
    });



});
