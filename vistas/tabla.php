<?php



?>

 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <meta charset="utf-8">
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
     <title></title>
   </head>
   <body>


      <table class="table table-danger">
          <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">idioma</th>
                <th scope="col">nivel</th>
                <th scope="col"></th>
              </tr>
          </thead>
          <tbody id="bodyIdiomas">

          </tbody>
      </table>

      <form id="formularioIdiomas">
          <input type="hidden" name="id" id="id" value=2> <!-- agregar el id value=<= $_GET["id"]?> -->
          <div class="card mt-5">
              <div class="card-header " style="background: #7fb3d5;">
                  <h5>Lengua extrangera</h5>
              </div>
              <div class="card-body">
                  <div class="row mt-2">
                      <label for="idioma">Idioma</label>
                      <input type="text" class="form-control" id="idioma" name="idioma" placeholder="Idioma">
                  </div>
                  <div class="row mt-2">
                      <label for="nivelIdioma">Nivel del idioma</label>
                      <select class="form-control" id="nivelIdioma" name="nivelIdioma">
                          <option value="A0: Principiante">A0: Principiante</option>
                          <option value="A1-A2: Básico'">A1-A2: Básico</option>
                          <option value="A2-B1: Pre-intermedio">A2-B1: Pre-intermedio</option>
                          <option value="B1: Intermedio">B1: Intermedio</option>
                          <option value="C1-C2: Avanzado">C1-C2: Avanzado</option>
                      </select>
                  </div>

              </div>
              <div class="card-footer text-muted">
                  <div class="row">
                      <div class="col-3" style=" text-align:center;">
                          <button class="btn btn-primary" type="submit">Guardar</button>
                          <a class="btn btn-success" href="menuAdmin.php" type="button">Cancelar</a>
                      </div>

                  </div>
              </div>
          </div>


      </form>





      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
      <script src="../js/jquery-3.6.2.min.js"></script>
      <script src="../js/tabla.js"></script>
     </body>
 </html>
