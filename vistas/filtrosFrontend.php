<?php
require "../controladores/controladorSession.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">IAES</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="menuAdmin2.php">Inicio</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Filtros de búsqueda
                </a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#">Filtros</a></li>
                  <li><a class="dropdown-item" href="#">Ayuda</a></li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="#">Sobre Nosotros</a></li>
                </ul>
            </ul>
            <form class="d-flex" role="search">
              <input class="form-control me-2" type="search" placeholder="Busqueda por nombre" aria-label="Search">
              <button class="btn btn-outline-secondary" type="submit">Buscar</button>
            </form>
          </div>
        </div>
    </nav>


    <div class="container p-4 mt-5">
      <form id="formulario">
        <div class="border border-primary">
            <div class="row">
              <h1 class="text-center">FILTRO DE BUSQUEDA</h1>
            </div>

            <div class="row p-4">
                <div class="col-4">

                  <div>
                  <label class="h6"> Carreras</label>
                      <select class="form-select" aria-label="Default select example" id="carreras" name="carreras">
                          <option selected value=""> Seleccionar carrera</option>
                          <option value=1 >analista de sistemas</option>
                          <option value=4>Recursos humanos</option>
                          <option value=2>Administracion de empresas</option>
                          <option value=5>Regimen aduanero</option>
                          <option value=6>Turismo y gestion hotelera</option>
                          <option value="v"></option>
                      </select>
                  </div>
                  <div class="mt-3">
                      <label class="h6">Capacidad del egresado?</label>
                      <select class="form-select" name="capacidad" id="capacidad">
                          <option value="mañana">Mañana</option>
                      </select>
                  </div>
                  <div class="mt-3">
                    <label class="h6">Disponibilidad horaria</label>
                    <select class="form-select" name="horario" id="horario" center="align">
                      <option value="tiempoc">Tiempo completo</option>
                      <optgroup label="Medio tiempo :">
                        <option value="mañana">Mañana</option>
                        <option value="tarde">Tarde</option>
                        <option value="noche">Noche</option>
                      </optgroup>
                    </select>
                  </div>

                </div>

                <div class="col-4">
                  <div>
                      <label class="h6" >Genero</label>
                      <select class="form-select" name="genero" id="genero">
                        <option value="">Excluyente</option>
                        <option value="masculino">Masculino</option>
                        <option value="femenino">Femenino</option>
                        <option value="Otro">Otro</option>
                      </select>
                  </div>

                  <div class="mt-3">
                    <label class="h6">Edad</label>
                    <select class="form-select" name="edad" id="edad">
                      <option value="1"> Mayor de 18 hasta 30</option>
                      <option value="2">Mayor de 30 hasta 50</option>
                      <option value="3">Mayor de 50 hasta 65</option>
                      <option value="4">Mayor de 18 hasta 65</option>
                    </select>
                  </div>

                  <div class="mt-3">
                    <label class="h6">Estado civil</label>
                    <select class="form-select" name="estadoCivil" id="estadoCivil">
                      <option value="">Excluyente</option>
                      <option value="Casado">Casado/a</option>
                      <option value="Soltero">Soltero/a</option>
                      <option value="Divorciado">Divorciado</option>
                      <option value="Viudo">Viudo</option>
                    </select>
                  </div>

              </div>


                <div class="col-4">
                  <div class="mt-3">
                    <label>Localidad</label>
                    <select class="form-select" name="localidad" id="localidad" center="align">
                      <option selected value=""> Excluyente</option>
                      <option value="puerto rico">Puerto Rico</option>
                      <option value="capiovi">Capiovi</option>
                      <option value="posadas">Posadas</option>
                    </select>
                  </div>

                  <div class="form-check">
                    <br>
                    <input class="form-check-input" name="manejo" type="checkbox" id="flexCheckDefault">
                    <label class="form-check-label h6"for="flexCheckDefault">
                      Manejo de automovil
                    </label>
                  </div>

                  <div class="form-check">
                    <br>
                    <input class="form-check-input" name="experiencia"type="checkbox" id="flexCheckDefault">
                    <label class="form-check-label h6"for="flexCheckDefault">
                      Experiencia laboral
                    </label>
                  </div>

                </div>



            </div>
            <div style="display: flex; align-items: center; justify-content: center;">
                <div class="row p-4 col-3" >
                    <input href="#" type="submit" class="btn btn-primary " id="buscar"  value="Buscar">
                </div>
            </div>
        </div>
      </form>
    </div>



    <div class="container mt-5">
      <div class="row">
          <div class="col-sm-12 col-m-12 col-xl-12 col-lg-12">
              <div class="table-responsive">
              <table class="table table table-dark table-hover text-center">
                  <thead class="bg-info">
                      <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Carrera</th>
                      <th scope="col">Genero</th>
                      <th scope="col">Edad</th>
                      <th scope="col-4">Estadocivil</th>
                      <th scope="col">Localidad</th>
                      <th scope="col">Disponibilidad</th>
                      <th scope="col">Conduce</th>
                      <th scope="col">Foto</th>
                      <th scope="col"></th>
                      </tr>
                  </thead>
                  <tbody id="tabla">

                  </tbody>

              </table>
              </div>
          </div>
      </div>
  </div>








    <script src="../js/jquery-3.6.2.min.js"></script>
    <script src="../js/filtros.js"></script>
    <script src="../bootstrap/js/bootstrap.bundle.min.js" ></script>

</body>
</html>
