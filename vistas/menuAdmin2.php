<?php
require "../controladores/controladorSession.php";
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menu</title>
    <link rel="stylesheet" href="../estilos/menuAdmin.css">

    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!--link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous"-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">


</head>
<body>

    <?php
    require "navbar.php";
    ?>


<section id="menu">




    <a href="cargarUsuario.php">
        <div class="contenedor " id="uno">

            <img class="icon" src="../imagenes/carga-de-archivos.png" style="width: 90px;" >
            <p class="texto">Carga de datos Alumno</p>


        </div>
    </a>

    <a href="filtrosFrontend.php">
        <div class="contenedor" id="dos">
            <img class="icon" src="../imagenes/busqueda-de-trabajo (1).png" style="width: 90px;" >
            <p class="texto">Filtros</p>

        </div>
    </a>

    <a href="#">
        <div class="contenedor" id="tres">

            <img class="icon" src="../imagenes/perfil-del-usuario.png " style="width: 90px;">
            <p class="texto">Logins</p>

        </div>
    </a>

    <a href="altaUser.php">
        <div class="contenedor" id="cuatro">
            <img class="icon" src="../imagenes/equipo.png" style="width: 90px;" >
            <p class="texto">Altas de Usuario</p>
        </div>
    </a>

    <a href="#">
        <div class="contenedor" id="cinco">
            <img class="icon" src="../imagenes/edificio.png"style="width: 90px;" >
            <p class="texto">Empresas</p>

        </div>
    </a>

    <a href="#">
    <div class="contenedor " id="seis">
        <img class="icon" src="../imagenes/puerta-de-salida.png" style="width: 90px;" >
        <p class="texto">Cambiar contraseña</p>

    </div>
    </a>


</section>






    <!--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#FE930AC9" fill-opacity="1" d="M0,128L288,288L576,64L864,224L1152,288L1440,0L1440,320L1152,320L864,320L576,320L288,320L0,320Z"></path></svg>-->
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#fe930a" fill-opacity="1" d="M0,0L60,32C120,64,240,128,360,149.3C480,171,600,149,720,149.3C840,149,960,171,1080,192C1200,213,1320,235,1380,245.3L1440,256L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"></path></svg>
    <!-- JavaScript Bundle with Popper -->
    <script src="../bootstrap/js/bootstrap.bundle.min.js" ></script>
</body>

<footer>
    <body>


        <div class="container-fluid">
            <div class="row p-5 pb-1 bg-dark ">
                <div class="col-xs-12 col-md-6 col-lg-3">

                    <div>
                        <a class="h3 text-white text-decoration-none" href="http://www.iaes.edu.ar/">
                            IAES
                        </a>
                    </div>

                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <p class="h5 text-light">Sobre nosotros</p>
                    <div class="mb-3">
                        <a class="text-secondary text-decoration-none" href="http://www.iaes.edu.ar/sobre/">Más informacion</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <p class="h5 text-light">Ayuda</p>
                    <div>
                    <a href="http://www.iaes.edu.ar/contacto/" class="text-secondary text-decoration-none">
                        Acceder
                    </a>
                    </div>
                    <div>
                    <a href="https://goo.gl/maps/DMiLRVpcBgXeGpzK9" class="text-secondary text-decoration-none">
                        Ubicacion
                    </a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <p class="h5 text-white">Contacto</p>
                <div>
                    <a href="https://www.facebook.com/iaespuertorico" class=" text-secondary text-decoration-none" >
                        Facebook
                    </a>
                </div>
                <div>
                    <a href="https://api.whatsapp.com/send/?phone=543743555338&text&type=phone_number&app_absent=0" class=" text-secondary text-decoration-none" >
                        Whatsapp
                    </a>
                </div>
                <div>
                    <a href="#" class=" text-secondary text-decoration-none" >
                        Instagram
                    </a>
                </div>
                </div>
                <div class="col-xs-12">
                    <p class="text-white text-center">Copyright -All rights reserved © 2022 </p>
                </div>
            </div>
        </div>
    </body>
</footer>


</html>
