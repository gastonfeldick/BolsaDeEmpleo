<?php
include "../controladores/controladorSession.php"

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alumnos</title>
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">IAES</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="menuAdmin.php">Inicio</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Filtros de búsqueda
              </a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#">Filtros</a></li>
                <li><a class="dropdown-item" href="#">Ayuda</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="#">Sobre Nosotros</a></li>
              </ul>
          </ul>
          <form class="d-flex" role="search">
            <input class="form-control me-2" type="search" placeholder="Busqueda por nombre" aria-label="Search">
            <button class="btn btn-outline-secondary" type="submit">Buscar</button>
          </form>
        </div>
      </div>
    </nav>

    <div class="container">
        <div class="row">

            <div class="col-sm-8 col-m-8 col-xl-8 col-lg-8">
                <p class="h1 text-center"> Seleccione el alumno que desea cargar </p>
                <div class="table-responsive">
                <table class="table table table-dark table-hover text-center">
                    <thead class="bg-info">
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Usuario</th>
                        <th scope="col">tipo</th>
                        <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php
                        include "../controladores/conexiondb.php";
                        if($_POST){


                        }else {
                            //QUERY SELECT datosalumno.id_alumno,usuario.usuario,usuario.tipo_usuario FROM datosalumno,usuario WHERE datosalumno.nombre='' AND datosalumno.id_alumno=usuario.id_alumno;
                            $sql=$conexion->query("SELECT * FROM Usuario WHERE tipo_usuario='alumno' ");
                            while($datos=$sql->fetch_object()){?>


                            <tr>
                                <td><?= $datos->id_usuario ?></td>
                                <td><?= $datos->usuario?></td>
                                <td><?= $datos->tipo_usuario?></td>
                                <td>
                                    <a href="datosUsuario.php?id=<?=$datos->id_usuario?>" class="btn btn-small btn-info" name="btn-curriculum"> <i class="bi bi-pencil-square"></i></a>
                                </td>
                            </tr>
                <?php
                        }
                    }

                ?>
                    </tbody>

                </table>
                </div>
            </div>
        </div>
    </div>

















    <script src="../bootstrap/js/bootstrap.bundle.min.js" ></script>
</body>

<footer>
    <body>
        <!--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#FE930AC9" fill-opacity="1" d="M0,128L288,288L576,64L864,224L1152,288L1440,0L1440,320L1152,320L864,320L576,320L288,320L0,320Z"></path></svg>-->
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#fe930a" fill-opacity="1" d="M0,0L60,32C120,64,240,128,360,149.3C480,171,600,149,720,149.3C840,149,960,171,1080,192C1200,213,1320,235,1380,245.3L1440,256L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"></path></svg>
        <!-- JavaScript Bundle with Popper -->

        <div class="container-fluid">
            <div class="row p-5 pb-1 bg-dark ">
                <div class="col-xs-12 col-md-6 col-lg-3">

                    <div>
                        <a class="h3 text-white text-decoration-none" href="http://www.iaes.edu.ar/">
                            IAES
                        </a>
                    </div>

                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <p class="h5 text-light">Sobre nosotros</p>
                    <div class="mb-3">
                        <a class="text-secondary text-decoration-none" href="http://www.iaes.edu.ar/sobre/">Más informacion</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <p class="h5 text-light">Ayuda</p>
                    <div>
                    <a href="http://www.iaes.edu.ar/contacto/" class="text-secondary text-decoration-none">
                        Acceder
                    </a>
                    </div>
                    <div>
                    <a href="https://goo.gl/maps/DMiLRVpcBgXeGpzK9" class="text-secondary text-decoration-none">
                        Ubicacion
                    </a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <p class="h5 text-white">Contacto</p>
                <div>
                    <a href="https://www.facebook.com/iaespuertorico" class=" text-secondary text-decoration-none" >
                        Facebook
                    </a>
                </div>
                <div>
                    <a href="https://api.whatsapp.com/send/?phone=543743555338&text&type=phone_number&app_absent=0" class=" text-secondary text-decoration-none" >
                        Whatsapp
                    </a>
                </div>
                <div>
                    <a href="#" class=" text-secondary text-decoration-none" >
                        Instagram
                    </a>
                </div>
                </div>
                <div class="col-xs-12">
                    <p class="text-white text-center">Copyright -All rights reserved © 2022 </p>
                </div>
            </div>
        </div>
    </body>
</footer>



</html>
