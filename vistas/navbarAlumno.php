<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Document</title>
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">IAES</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">Inicio</a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            <?php

                session_start();

                $aux=0; 
                if($_SESSION){
                    if($_SESSION['tipo']=='alumno'){
                        echo"Alumno";
                        $aux=1;
                    }elseif($_SESSION['tipo']=='admin'){
                        echo"admin";
                        $aux=2;
                    }
                }else{
                    echo"ingresar";
                }
            ?>
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="../controladores/cerrarsession.php">Cerrar sesion</a></li>
            <li><a class="dropdown-item" href="#">Ayuda</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Sobre Nosotros</a></li>
          </ul> 
        </li>
      </ul>
    </div>
  </div>
</nav>  


<script src="../bootstrap/js/bootstrap.bundle.min.js" ></script>
</body>
</html>