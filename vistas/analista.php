<?php
include("../controladores/conexiondb.php")?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
<link rel="stylesheet" href="estilo.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">IAES</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php">Inicio</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Informacion
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="#">Ayuda</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Sobre Nosotros</a></li>
          </ul> 
      </ul>
      <form class="d-flex" role="search">
        <input class="form-control me-2" type="search" placeholder="Busqueda por nombre" aria-label="Search">
        <button class="btn btn-outline-secondary" type="submit">Buscar</button>
      </form>
    </div>
  </div>
</nav>  
    <h1 class="text-center">Analista</h1>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-m-12 col-xl-12 col-lg-12">
                <table class="table table table-dark table-hover text-center">
                    <thead>
                    <tr>
                        <td>Nombre</td>
                        <td>Apellido</td>
                        <td>Dni</td>
                        <td>Genero</td>
                        <td>Nacionalidad</td>
                        <td>Acciones</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <?php
                        $sql = "SELECT nombre,apellido,dni,genero,nacionalidad FROM DatosAlumno WHERE id_carrera='4'";
                        $resultado = mysqli_query($conexion,$sql);
                        while($mostrar=mysqli_fetch_array($resultado)){
                            ?>
                        
                        
                        <td><?php echo $mostrar['nombre']?></td>
                        <td><?php echo $mostrar['apellido']?></td>
                        <td><?php echo $mostrar['dni']?></td>
                        <td><?php echo $mostrar['genero']?></td>
                        <td><?php echo $mostrar['nacionalidad']?></td>
                        <td class="text-center">
                        <a href="index.php?id=<?php echo $row['id']?>" class="btn btn-outline-light"> <!--obtengo el id de esta forma-->
                                                Ver curriculum
                        </a>
                        <a href="index.php?id=<?php echo $row['id']?>" class="btn btn-outline-light"> <!--obtengo el id de esta forma-->
                                                Ver Perfil
                        </a>
                        </td>
                    </tr>
                    <?php

                        }
                        ?>
                    </tbody>
                   
                </table>
            </div>
        </div>
    </div>
</body>
<?xml version="1.0" standalone="no"?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#fe930a" fill-opacity="1" d="M0,192L48,202.7C96,213,192,235,288,229.3C384,224,480,192,576,181.3C672,171,768,181,864,165.3C960,149,1056,107,1152,101.3C1248,96,1344,128,1392,144L1440,160L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path></svg>

<footer>
<body>
    <div class="container-fluid">
        <div class="row p-5 pb-1 bg-dark">
            <div class="col-xs-12 col-md-6 col-lg-3">
                
                <div>
                    <a class="h3 text-white text-decoration-none" href="http://www.iaes.edu.ar/">
                        IAES
                    </a>
                </div>
                
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                <p class="h5 text-light">Sobre nosotros</p>
                <div class="mb-3">
                    <a class="text-secondary text-decoration-none" href="http://www.iaes.edu.ar/sobre/">Más informacion</a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                <p class="h5 text-light">Ayuda</p>
                <div>
                <a href="http://www.iaes.edu.ar/contacto/" class="text-secondary text-decoration-none">
                    Acceder
                </a>
                </div>
                <div>
                <a href="https://goo.gl/maps/DMiLRVpcBgXeGpzK9" class="text-secondary text-decoration-none">
                    Ubicacion
                </a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                <p class="h5 text-white">Contacto</p>
            <div>
                <a href="https://www.facebook.com/iaespuertorico" class=" text-secondary text-decoration-none" >
                    Facebook
                </a>
            </div>
            <div>
                <a href="https://api.whatsapp.com/send/?phone=543743555338&text&type=phone_number&app_absent=0" class=" text-secondary text-decoration-none" >
                    Whatsapp
                </a>
            </div>
            <div>
                <a href="#" class=" text-secondary text-decoration-none" >
                    Instagram
                </a>
            </div>
            </div>
            <div class="col-xs-12">
                <p class="text-white text-center">Copyright -All rights reserved © 2022 </p>
            </div>
        </div>
    </div>
</body>
</footer>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</html>