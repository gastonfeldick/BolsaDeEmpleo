<?php


require "../controladores/controladorSession.php";
require "../controladores/conexiondb.php";





$argumento="ss";

if(empty($argumento)){
    echo('vacio');
}else{
    echo("cargadp");

}

if(isset($_GET['id'])){
    $resultado=$conexion->query("SELECT * FROM `Provincia`;"); //listado provincia
    if($_SESSION['tipo']=="alumno"){
      echo "soy Alumno";
    }
      $id=$_GET['id'];


    $usuario=$conexion->query("SELECT * FROM `Usuario` WHERE `id_usuario`=$id;");
    $datos=$conexion->query("SELECT * FROM `DatosAlumno` WHERE `id_usuario`=$id;");
    $carrera=$conexion->query("SELECT * FROM `Carrera`; ");


    if(!empty($datos->fetch_object())){
        //funcion rellenar
        echo("hola");
    }else{
        echo"no tiene cargado";
    }




}




?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>formulario</title>
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kanit&display=swap" rel="stylesheet">
    <script src="../js/jquery-3.6.2.min.js"></script>
<body>

    <?php
    require "navbar.php";
    ?>

    <!-- FORMULARIO  -->

    <h1 class=" p-4 text-center"> Datos Personales</h1>

    <div class="container" style="font-family: 'Kanit', sans-serif;">
        <form id="formulario">
            <input type="hidden" name="id" id="id" value=<?= $_GET["id"]?>>

        <div class="card">
            <div class="card-header" style="background: #7fb3d5;">
                <h5>
                    ID Alumno: <?php  if($usuario){

                            echo($id);    }
                    ?>

                </h5>
                <h5> Usuario: <?php  echo($usuario->fetch_object()->usuario); ?></h5>



            </div>
            <div class="card-body">


                <div class="row">
                    <div class="col-4">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name=nombre id="nombre" placeholder="Nombre" required>
                    </div>
                    <div class="col-4">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control"  name="apellido" id="apellido" placeholder="Apellido" required>
                    </div>
                    <div class="col-4">
                        <label for="dni">Dni</label>
                        <input type="text" class="form-control" name="dni" id="dni" placeholder="Dni" required>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-4">
                        <label for="fecha">Fecha de Nacimiento</label>
                        <input type="date" class="form-control" name="fecha" id="fecha" placeholder="Nombre" required>
                    </div>
                    <div class="col-4 form-group">
                        <label for="genero">Genero</label>
                        <select class="form-control" id="genero" name="genero">
                            <option value="Masculino">Masculino</option>
                            <option value="Femenino">Femenino</option>
                            <option value="Otro">Otro</option>
                        </select>
                    </div>
                    <div class="col-4 form-group">
                        <label for="estadoCivil">Estado Civil</label>
                        <select class="form-control" id="estadoCivil" name="estadoCivil"placeholder="elegir">
                            <option value="Soltero">Soltero/a</option>
                            <option value="Casado">Casado/a</option>
                            <option value="Divorciado">Divorciado</option>
                            <option value="Viudo">Viudo</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col">
                        <label for="hijos">Cantidad de hijos</label>
                        <select class="form-control" id="hijos" name="hijos" placeholder="elegir">
                            <option value="0">No tengo</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">mas de 6</option>
                        </select>
                    </div>
                    <div class="col-4">
                        <label for="nacionalidad">Nacionalidad</label>
                        <input type="text" class="form-control" id="nacionalidad" name="nacionalidad" placeholder="Nacionalidad" required>
                    </div>
                    <div class="col-4">
                    <label for="discapacidad">Discapacidad</label>
                        <select class="form-control" id="discapacidad" name="discapacidad">
                            <option value="0">No</option>
                            <option value="1">Si</option>
                        </select>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-4">
                        <label for="localidad">Localidad</label>
                        <input type="text" class="form-control" id="localidad" name="localidad" placeholder="Localidad" required>
                    </div>
                    <div class="col-4">
                        <label for="provincias">Provincia</label>
                        <select class="form-control" id="provincias" name="provincias" required>
                            <?php
                            while($row=$resultado->fetch_object()){
                            ?>
                                <option value=<?=$row->id_provincia?>><?php echo($row->provincia);?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-4">
                        <label for="pais">Pais</label>
                        <input type="text" class="form-control" id="pais" name="pais" placeholder="pais" required>
                    </div>
                </div>


                    <div class="row mt-4">

                        <div class="col-4">
                            <label for="codigoPostal">Código Postal</label>
                            <input type="text" class="form-control" id="codigoPostal" name="codigoPostal" placeholder="Código Postal" required>
                        </div>

                        <div class="col-4">
                            <label for="grupoSanguineo">Grupo sanguineo</label>
                                <select class="form-control" id="grupoSanguineo" name="grupoSanguineo">
                                    <option value="A negativo">A negativo</option>
                                    <option value="A positivo">A positivo</option>
                                    <option value="B negativo">B negativo</option>
                                    <option value="B positivo">B positivo</option>
                                    <option value="O negativo">O negativo</option>
                                    <option value="O positivo">O positivo</option>
                                    <option value="AB negativo">AB negativo</option>
                                    <option value="AB positivo">AB positivo</option>
                                </select>
                        </div>

                        <div class="col-4">
                            <label for="carrera">Carrera</label>
                            <select class="form-control" id="carrera" name="carrera" required>
                                <?php
                                while($row=$carrera->fetch_object()){
                                ?>
                                    <option value=<?=$row->id_carrera?>><?php echo($row->carrera);?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>

                    </div>


                    <div class="row mt-4">

                        <div class="col-4">
                        <label for="licenciaConducir">Licencia de conducir:</label>
                            <select class="form-control" id="licenciaConducir" name="licenciaConducir">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>


                    </div>








            </div>

                <div class="card-footer text-muted">
                    <div class="row">
                        <div class="col-3" style=" text-align:center;">
                            <button class="btn btn-primary" type="submit">Guardar</button>
                            <a class="btn btn-danger" id="cancelar"name="cancelar" href="menuAdmin.php" type="button">Cancelar</a>

                        </div>

                    </div>
                </div>

        </div>
        </form>

        <!-- formulario -->
        <!-- formularioContacto -->

        <form id="formularioContacto">
            <input type="hidden" name="id" id="id" value=<?= $_GET["id"]?>>
            <div class="card mt-5">
                <div class="card-header" style="background: #7fb3d5;">
                    <h5>Contacto</h5>
                </div>
                <div class="card-body">

                    <div class="row">
                        <label for="correo">Correo</label>
                        <input type="text" class="form-control" id="correo" name="correo" placeholder="Correo">
                    </div>
                    <div class="row mt-2">
                        <label for="telefono">Telefono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono">
                    </div>
                    <div class="row mt-2">
                        <label for="telefonoFijo">Telefono Fijo</label>
                        <input type="text" class="form-control" id="telefonoFijo" name="telefonoFijo" placeholder="Telefono Fijo">
                    </div>
                    <div class="row mt-2">
                        <label for="telefonoFijo">Facebook</label>
                        <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Ingrese el link de su Facebook">
                    </div>
                    <div class="row mt-2">
                        <label for="linkedin">Linkedin</label>
                        <input type="text" class="form-control" id="linkedin" name="linkedin" placeholder="Telefono Fijo">
                    </div>
                </div>
                <div class="card-footer text-muted">
                    <div class="row">
                        <div class="col-4" style=" text-align:center;">
                            <button class="btn btn-primary" type="submit">Guardar</button>
                            <a class="btn btn-danger" href="menuAdmin.php" type="button">Cancelar</a>
                            <button class="btn btn-success" id="siguiente"> Siguiente</button>
                        </div>

                    </div>
                </div>
            </div>
        </form>

        <!-- formularioCarrera iaes-->

            <div class="card mt-5">
                <div class="card-header " style="background: #7fb3d5;">
                    <h5> Titulos obtenidos en el Iaes</h5>
                </div>
                <div class="card-body">
                  <!-- Todas la carreras-->
                      <table class="table table-danger text-center">
                          <thead>
                              <tr>
                                <th scope="col">ID Carrera</th>
                                <th scope="col">Carrera</th>
                                <th scope="col"></th>
                              </tr>
                          </thead>
                          <tbody id="bodyCarreras">

                          </tbody>
                      </table>
                  <!---->
                  <form id="formularioCarrera">
                      <input type="hidden" name="id" id="id" value=<?= $_GET["id"]?>>
                  <div class="form-check">
                      <input class="form-check-input" type="checkbox" name="analista" value="1" id="analista">
                      <label class="form-check-label" for="defaultCheck1">
                          Técnico Superior Analista de Sistemas de Computación
                      </label>

                  </div>
                  <div class="form-check">
                      <input class="form-check-input" type="checkbox" name="administracion" value="2" id="administracion">
                      <label class="form-check-label" for="defaultCheck1">
                          Técnico Superior en Administración de Empresas
                      </label>

                  </div>
                  <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="3" name="gastronomia" id="gastronomia">
                      <label class="form-check-label" for="defaultCheck1">
                          Técnico Superior en Gastronomía
                      </label>

                  </div>
                  <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="4" name="recursos" id="recursos">
                      <label class="form-check-label" for="defaultCheck1">
                          Técnico Superior en Gestión de Recursos Humanos
                      </label>

                  </div>
                  <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="5" name="regimen" id="regimen">
                      <label class="form-check-label" for="defaultCheck1">
                          Técnico Superior en Régimen Aduanero
                      </label>

                  </div>
                  <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="6" name="turismo" id="turismo">
                      <label class="form-check-label" for="defaultCheck1">
                          Técnico Superior en Turismo y Gestión Hotelera
                      </label>

                  </div>
                  <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="7" name ="tecnico"id="tecnico">
                      <label class="form-check-label" for="defaultCheck1">
                          Técnico Superior y Soporte Técnico de Computadoras
                      </label>

                  </div>


                </div>
                <div class="card-footer text-muted">
                    <div class="row">
                        <div class="col-4" style=" text-align:center;">
                            <button class="btn btn-primary" type="submit">Guardar</button>
                            <a class="btn btn-danger" href="menuAdmin.php" type="button">Cancelar</a>
                            <button class="btn btn-success" id="siguiente"> Siguiente</button>
                        </div>
                    </div>
                </div>

            </div>

        </form>

        <form id="formularioAcademicoSecundaria">
            <input type="hidden" name="id" id="id" value=<?= $_GET["id"]?>>
            <div class="card mt-5">
                <div class="card-header " style="background: #7fb3d5;">
                    <h5> Formacion Secundaria</h5>
                    <h6 style="color:#e65454;"> Si curso en más de una institución solo ingresar la última </h1>
                </div>
                <div class="card-body">
                    <div class="row mt-2">
                        <label for="instituto">Instituto</label>
                        <input type="text" class="form-control" id="instituto" name="instituto" placeholder="Instituto o escuela">
                    </div>
                    <div class="row mt-2">
                        <label for="titulo">Titulo</label>
                        <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Titulo">
                    </div>
                    <div class="row mt-2">
                        <label for="desde">Año de inicio</label>
                        <input type="number" class="form-control" id="desde" name="desde" placeholder="Año de inicio" required>
                    </div>
                    <div class="row mt-2">
                        <label for="hasta">Año de Fin</label>
                        <input type="number" class="form-control" id="hasta" name="hasta" placeholder="Año de fin">
                    </div>

                </div>
                <div class="card-footer text-muted">
                    <div class="row">
                        <div class="col-4" style=" text-align:center;">
                            <button class="btn btn-primary" type="submit">Guardar</button>
                            <a class="btn btn-danger" href="menuAdmin.php" type="button">Cancelar</a>
                            <button class="btn btn-success" id="siguiente"> Siguiente</button>
                        </div>
                    </div>
                </div>

            </div>

        </form>


            <div class="card mt-5">
                <div class="card-header " style="background: #7fb3d5;">
                    <h5> Formacion Universitaria</h5>
                    <h6 style="color:#e65454;"> Si solo tiene tiene titulos del iaes omitir este formulario </h1>
                </div>
                <div class="card-body">
                  <!-- Todos los idiomas del user-->
                      <table class="table table-danger text-center">
                          <thead>
                              <tr>
                                <th scope="col">Institución</th>
                                <th scope="col">Titulo</th>
                                <th scope="col">Inicio</th>
                                <th scope="col">FIn</th>
                                <th scope="col"></th>
                              </tr>
                          </thead>
                          <tbody id="bodyUniversitario">

                          </tbody>
                      </table>
                  <!---->
                    <form id="formularioAcademicoUniversitario">
                      <input type="hidden" name="id" id="id" value=<?= $_GET["id"]?>>
                    <div class="row mt-2">
                        <label for="universidad">Universidad o Instituto</label>
                        <input type="text" class="form-control" id="universidad" name="universidad" placeholder="Universidad o Instituto" required>
                    </div>
                    <div class="row mt-2">
                        <label for="titulo">Titulo</label>
                        <input type="text" class="form-control" id="tituloUniversitario" name="tituloUniversitario" placeholder="Titulo" required>
                    </div>
                    <div class="row mt-2">
                        <label for="desde">Año de inicio</label>
                        <input type="number" class="form-control" id="desdeUniversitario" name="desdeUniversitario" placeholder="Año de inicio" required>
                    </div>
                    <div class="row mt-2">
                        <label for="universidad">Año de Fin</label>
                        <input type="number" class="form-control" id="hastaUniversitario" name="hastaUniversitario" placeholder="Universidad o Instituto" required>
                    </div>

                </div>
                <div class="card-footer text-muted">
                    <div class="row">
                        <div class="col-4" style=" text-align:center;">
                            <button class="btn btn-primary" type="submit">Guardar</button>
                            <a class="btn btn-danger" href="menuAdmin.php" type="button">Cancelar</a>
                            <button class="btn btn-success" id="siguiente"> Siguiente</button>
                        </div>
                    </div>
                </div>

            </div>

        </form>


            <div class="card mt-5">
                <div class="card-header " style="background: #7fb3d5;">
                    <h5>Lengua extrangera</h5>
                </div>
                <div class="card-body">
                    <!-- Todos los idiomas del user-->
                        <table class="table table-danger text-center">
                            <thead>
                                <tr>
                                  <th scope="col">ID Usuario</th>
                                  <th scope="col">idioma</th>
                                  <th scope="col">nivel</th>
                                  <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody id="bodyIdiomas">

                            </tbody>
                        </table>
                    <!---->
                    <form id="formularioIdiomas">
                        <input type="hidden" name="id" id="id" value=<?= $_GET["id"]?>>
                    <div class="row mt-2">
                        <label for="idioma">Idioma</label>
                        <input type="text" class="form-control" id="idioma" name="idioma" placeholder="Idioma">
                    </div>
                    <div class="row mt-2">
                        <label for="nivelIdioma">Nivel del idioma</label>
                        <select class="form-control" id="nivelIdioma" name="nivelIdioma">
                            <option value="A0: Principiante">A0: Principiante</option>
                            <option value="A1-A2: Básico'">A1-A2: Básico</option>
                            <option value="A2-B1: Pre-intermedio">A2-B1: Pre-intermedio</option>
                            <option value="B1: Intermedio">B1: Intermedio</option>
                            <option value="C1-C2: Avanzado">C1-C2: Avanzado</option>
                        </select>
                    </div>

                </div>
                <div class="card-footer text-muted">
                    <div class="row">
                        <div class="col-3" style=" text-align:center;">
                            <button class="btn btn-primary" type="submit">Guardar</button>
                            <a class="btn btn-success" href="menuAdmin.php" type="button">Cancelar</a>
                        </div>

                    </div>
                </div>
            </div>

            <script id="scrip">

            </script>


        </form>



            <div class="card mt-5">
                <div class="card-header"  style="background: #7fb3d5;">
                    <h5>Conocimiento Informatico</h5>
                </div>
                <div class="card-body">
                    <!-- Todos los programas del user-->
                        <table class="table table-danger text-center">
                            <thead>
                                <tr>
                                  <th scope="col">ID Usuario</th>
                                  <th scope="col">Programa</th>
                                  <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody id="bodyProgramas">

                            </tbody>
                        </table>
                    <!---->
                    <div class="row mt-4">
                        <form id="formularioConocimientoInformatico">
                        <input type="hidden" name="id" id="id" value=<?= $_GET["id"]?>>
                        <div class="col">
                            <label for="programa">Programa</label>
                            <input type="text" class="form-control" id="programa" name="programa" placeholder="¿Que programas manejas?">
                        </div>

                    </div>
                </div>
                <div class="card-footer text-muted">
                        <div class="row">
                            <div class="col-3" style=" text-align:center;">
                                <button class="btn btn-primary" type="submit">Guardar</button>
                                <a class="btn btn-success" href="menuAdmin.php" type="button">Cancelar</a>
                            </div>

                        </div>
                </div>
            </div>

        </form>


        <!--form id="formularioConocimientoHabilidad">

            <div class="card mt-5">
                <div class="card-header"  style="background: #7fb3d5;">
                    <h5>Conocimiento y habilidades</h5>
                </div>
                <div class="card-body">
                        <div class="row mt-3">
                            <label for="hablidad">Conocimiento y habilidades</label>
                            <input type="text" class="form-control" id="programa" name="programa" placeholder="Conocimiento y habilidad">
                        </div>
                        <div class="row mt-3">
                            <label for="hablidad">Conocimiento y habilidades</label>
                            <input type="text" class="form-control" id="programa" name="programa" placeholder="Conocimiento y habilidad">
                        </div>
                        <div class="row mt-3">
                            <label for="hablidad">Conocimiento y habilidades</label>
                            <input type="text" class="form-control" id="programa" name="programa" placeholder="Conocimiento y habilidad">
                        </div>
                </div>
                <div class="card-footer text-muted">
                        <div class="row">
                            <div class="col-3" style=" text-align:center;">
                                <button class="btn btn-primary" type="submit">Guardar</button>
                                <a class="btn btn-success" href="menuAdmin.php" type="button">Cancelar</a>
                            </div>

                        </div>
                </div>
            </div>

        </form-->

        <form id="formularioPerfilProfesional">
            <input type="hidden" name="id" id="id" value=<?= $_GET["id"]?>>
            <div class="card mt-5">
                <div class="card-header"  style="background: #7fb3d5;">
                    <h5>Perfil profesional</h5>
                </div>
                <div class="card-body">

                        <div class="row mt-3">
                            <div class="form-group">
                                <label for="cargo">Cargo</label>
                                <input type="text" class="form-control" id="cargo" name="cargo" placeholder=" Titulo del curriculum, EJ: ''Administrativo contable''">
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="form-group">
                                <label for="carta">Carta de Presentación</label>
                                <textarea class="form-control" id="carta" name="carta" rows="5" placeholder="Breve carta de presentación"></textarea>
                            </div>
                        </div>

                </div>
                <div class="card-footer text-muted">
                        <div class="row">
                            <div class="col-3" style=" text-align:center;">
                                <button class="btn btn-primary" type="submit">Guardar</button>
                                <a class="btn btn-success" href="menuAdmin.php" type="button">Cancelar</a>
                            </div>

                        </div>
                </div>
            </div>

        </form>

        <form id="formuluarioExperienciaLaboral">
            <input type="hidden" name="id" id="id" value=<?= $_GET["id"]?>>
            <div class="card mt-5">
                <div class="card-header" style="background: #7fb3d5;">
                    <h5>Experiencia Laboral</h5>
                </div>
                <div class="card-body">
                    <div class="row mt-3">
                        <div class="form-group">
                            <label for="puesto">Puesto</label>
                            <input type="text" class="form-control" id="puesto" name="puesto" placeholder="Puesto que cumplio en la empresa">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="form-group">
                            <label for="empresa">Empresa</label>
                            <input type="text" class="form-control" id="empresa" name="empresa" placeholder="Empresa donde trabajo">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="form-group">
                            <label for="tarea">Tarea</label>
                            <input type="text" class="form-control" id="tarea" name="tarea" placeholder="tarea que cumplio en la empresa">
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-4">
                            <label for="inicio">Fecha de inicio</label>
                            <input type="date" class="form-control" name="inicio" id="inicio" require>
                        </div>
                        <div class="col-4">
                            <label for="fin">Fecha de fin</label>
                            <input type="date" class="form-control" name="fin" id="fin"  require>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="form-group">
                            <label for="contacto">Contacto</label>
                            <input type="text" class="form-control" id="contacto" name="contacto" placeholder="Correo o telefono de la empresa">
                        </div>
                    </div>
                </div>
                <div class="card-footer text-muted">
                    <div class="row">
                        <div class="col-3" style=" text-align:center;">
                            <button class="btn btn-primary" type="submit">Guardar</button>
                            <a class="btn btn-success" href="menuAdmin.php" type="button">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>

        </form>


        <form id="formularioPreferenciaTrabajo">
            <div class="card mt-5">
                <div class="card-header" style="background: #7fb3d5;">
                    <h5> Preferencia laboral</h5>
                </div>
                <div class="card-body">
                    <div class="row mt-2">
                        <div class="form-group">
                            <label for="estado">Estado</label>
                            <select class="form-control" id="estado" name="estado">
                                <option value="Disponible">Disponible</option>
                                <option value="No Disponible">No Disponible</option>
                                <option value="A2-B1">No Disponible y En Busqueda</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="form-group">
                            <label for="horario">Disponibilidad horaria</label>
                            <select class="form-control" id="horario" name="horario">
                                <option value="Disponible">Completo</option>
                                <option value="No Disponible"> Mañana</option>
                                <option value="A2-B1">Tarde</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="form-group">
                            <label for="viajar">Disponibilidad para viajar</label>
                            <select class="form-control" id="viajar" name="viajar">
                                <option value="si">Si</option>
                                <option value="no"> no</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="form-group">
                            <label for="cambioResidencia">Disponibilidad para cambio de residencia</label>
                            <select class="form-control" id="cambioResidencia" name="cambioResidencia">
                                <option value="si">Si</option>
                                <option value="no"> no</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="card-footer text-muted">
                    <div class="row">
                        <div class="col-3" style=" text-align:center;">
                            <button class="btn btn-primary" type="submit">Guardar</button>
                            <a class="btn btn-success" href="menuAdmin.php" type="button">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>


        </form>

        <div class="card mt-5">
            <div class="card-header">
                Header
            </div>
            <div class="card-body">
                <form method="post" id="formCurriculum" class=" py-2" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="archivo" class="h3">Seleccione el archivo a enviar</label>
                        <label class="col-sm-2 control-label">Archivo</label>
                        <input type="file" class="form-control" id="archivo1" name="archivo1"><br>
                        <button class="btn btn-dark" name="submit">
                                Enviar nuevo curriculum
                        </button>
                    </div>
                </form>
            </div>
            <div class="card-footer text-muted">
                Footer
            </div>
        </div>

    </div>










    <!--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#FE930AC9" fill-opacity="1" d="M0,128L288,288L576,64L864,224L1152,288L1440,0L1440,320L1152,320L864,320L576,320L288,320L0,320Z"></path></svg>-->
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#fe930a" fill-opacity="1" d="M0,0L60,32C120,64,240,128,360,149.3C480,171,600,149,720,149.3C840,149,960,171,1080,192C1200,213,1320,235,1380,245.3L1440,256L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"></path></svg>
    <!-- JavaScript Bundle with Popper -->

    <script src="../bootstrap/js/bootstrap.bundle.min.js" ></script>

    <script src="../js/datosUser.js"></script>


</body>

<footer>
<body>
    <div class="container-fluid" >
        <div class="row p-5 pb-1 bg-dark">
            <div class="col-xs-12 col-md-6 col-lg-3">

                <div>
                    <a class="h3 text-white text-decoration-none" href="http://www.iaes.edu.ar/">
                        IAES
                    </a>
                </div>

            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                <p class="h5 text-light">Sobre nosotros</p>
                <div class="mb-3">
                    <a class="text-secondary text-decoration-none" href="http://www.iaes.edu.ar/sobre/">Más informacion</a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                <p class="h5 text-light">Ayuda</p>
                <div>
                <a href="http://www.iaes.edu.ar/contacto/" class="text-secondary text-decoration-none">
                    Acceder
                </a>
                </div>
                <div>
                <a href="https://goo.gl/maps/DMiLRVpcBgXeGpzK9" class="text-secondary text-decoration-none">
                    Ubicacion
                </a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                <p class="h5 text-white">Contacto</p>
            <div>
                <a href="https://www.facebook.com/iaespuertorico" class=" text-secondary text-decoration-none" >
                    Facebook
                </a>
            </div>
            <div>
                <a href="https://api.whatsapp.com/send/?phone=543743555338&text&type=phone_number&app_absent=0" class=" text-secondary text-decoration-none" >
                    Whatsapp
                </a>
            </div>
            <div>
                <a href="#" class=" text-secondary text-decoration-none" >
                    Instagram
                </a>
            </div>
            </div>
            <div class="col-xs-12">
                <p class="text-white text-center">Copyright -All rights reserved © 2022 </p>
            </div>
        </div>
    </div>
</body>
</footer>

</html>
