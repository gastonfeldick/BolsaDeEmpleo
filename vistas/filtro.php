<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">IAES</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Inicio</a>
                </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Filtros de búsqueda
                </a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#">Filtros</a></li>
                    <li><a class="dropdown-item" href="#">Ayuda</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="#">Sobre Nosotros</a></li>
                </ul>
            </ul>
            <form class="d-flex" role="search">
                <input class="form-control me-2" type="search" placeholder="Busqueda por nombre" aria-label="Search">
                <button class="btn btn-outline-secondary" type="submit">Buscar</button>
            </form>
            </div>
        </div>
    </nav>



    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-m-12 col-xl-12 col-lg-12">
                <div class="table-responsive">
                <table class="table table table-dark table-hover text-center">
                    <thead class="bg-info">
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Carrera</th>
                        <th scope="col">Genero</th>
                        <th scope="col">Edad</th>
                        <th scope="col-4">Estadocivil</th>
                        <th scope="col">Localidad</th>
                        <th scope="col">Disponibilidad</th>
                        <th scope="col">Conduce</th>
                        <th scope="col">Foto</th>
                        <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- revisar el while ,se abre y cierra php whats?-->
                        <?php

                            include ("../controladores/conexiondb.php");
                            print_r($_POST);
                            $carrera=$_POST["carreras"];
                            $capacidad=$_POST["capacidad"];
                            $horario=$_POST["horario"];
                            $manejo=$_POST["manejo"];
                            $experiencia=$_POST["experiencia"];
                            $localidad=$_POST["localidad"];
                            $genero=$_POST["genero"];
                            $edad=$_POST["edad"];
                            $estado_civil=$_POST["estadoCivil"];

                            if($_POST["carreras"]){
                                if($manejo=="si"){
                                    $query=("SELECT * FROM datosalumno,carrera,licencia,localidad WHERE datosalumno.id_carrera=$carrera AND datosalumno.id_carrera=carrera.id_carrera AND datosalumno.id_localidad=localidad.id_localidad");
                                }else{
                                    $query=("SELECT * FROM datosalumno,carrera,localidad WHERE datosalumno.id_carrera=$carrera AND datosalumno.id_carrera=carrera.id_carrera AND datosalumno.id_localidad=localidad.id_localidad");
                                }
                                #$query.="OR id_carrera =2";

                            }else{
                                if($manejo=="si"){
                                    $query=("SELECT * FROM datosalumno,carrera,licencia,localidad WHERE datosalumno.id_carrera=carrera.id_carrera AND datosalumno.id_localidad=localidad.id_localidad");
                                }else{
                                    $query=("SELECT * FROM datosalumno,carrera,localidad WHERE datosalumno.id_carrera=carrera.id_carrera AND datosalumno.id_localidad=localidad.id_localidad");
                                }
                            }

                            if($_POST["manejo"]){
                                $query.=(" AND licencia.id_alumno=datosalumno.id_alumno");
                            }

                            if($_POST["localidad"]){
                                $query.=(" AND localidad.nombre_localidad='$localidad'");

                            }
                            if($_POST["genero"]){
                                $query.=(" AND nombre_genero='$genero'");
                            }
                            if($_POST["estadoCivil"]){
                                $query.=(" AND nombre_estado_civil='$estado_civil'");
                            }
                            echo("<br><br>");

                            if(!$_POST["carreras"]){
                                $query.=(" ORDER BY carrera.id_carrera");
                            }
                            echo($query);
                            $sql=$conexion->query($query);


                            while($datos=$sql->fetch_object()){
                                $nacimiento = new DateTime($datos->fecha_nacimiento);
                                $ahora = new DateTime(date("Y-m-d"));
                                $diferencia = $ahora->diff($nacimiento);
                                $year=$diferencia->format("%y");

                                ## Filtro por edad

                                switch($edad){
                                    /* value="1"> Mayor de 18 hasta 30
                                       value="2"> Mayor de 30 hasta 50
                                       value="3"> Mayor de 50 hasta 65
                                       value="4"> Mayor de 18 hasta 65
                                    */
                                    case ($edad==1):
                                        $desde=18;
                                        $hasta=30;
                                        break;
                                    case ($edad==2):
                                        $desde=30;
                                        $hasta=50;
                                        break;
                                    case ($edad==3):
                                        $desde=50;
                                        $hasta=65;
                                        break;
                                    case ($edad==4):
                                        $desde=18;
                                        $hasta=65;
                                        break;
                                    default:
                                        break;
                                }


                                switch($datos->id_carrera){
                                    case 1:
                                        $nombreCarrera="Analista de Sistemas";
                                        break;
                                    case 2:
                                        $nombreCarrera="Administración de Empresas";
                                        break;
                                    case 3:
                                        $nombreCarrera="Gastronomía";
                                        break;
                                    case 4:
                                        $nombreCarrera="Recursos Humanos";
                                        break;
                                    case 5:
                                        $nombreCarrera="Régimen Aduanero";
                                        break;
                                    case 6:
                                        $nombreCarrera="Turismo y Gestión Hotelera";
                                        break;
                                    case 7:
                                        $nombreCarrera="Téc. Computadoras y Redes";
                                        break;
                                    default:
                                        break;
                                }


                                if($year>=$desde && $year<=$hasta){




                        ?>
                                        <tr>
                                            <td><?= $datos->id_alumno ?></td>
                                            <td><?= $datos->nombre ?></td>
                                            <td><?= $nombreCarrera ?></td>
                                            <td><?= $datos->nombre_genero?></td>
                                            <td><?= $year?></td>
                                            <td><?= $datos->nombre_estado_civil ?></td>
                                            <td><?= $datos->nombre_localidad ?></td>
                                            <td>disponible</td>
                                            <td>?</td>
                                            <td>foto</td>
                                            <td>
                                                <a href="verUsuario.php?id=<?=$datos->id_alumno?>" class="btn btn-primary "  name="btn-ver"> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
                                                <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
                                                <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
                                                </svg> </a>
                                                <a href="eliminar.php?id=<?=$datos->id_alumno?>" class="btn btn-small btn-danger" name="btn-modificar"><i class="bi bi-pencil-fill"></i></a>
                                                <a href="ver.php?id=<?=$datos->id_alumno?>" class="btn btn-small btn-info" name="btn-curriculum"> <i class="bi bi-file-text"></i></a>
                                            </td>
                                        </tr>
                        <?php
                                }
                            }
                        ?>







                    </tbody>

                </table>
                </div>
            </div>
        </div>
    </div>

    <script src="../bootstrap/js/bootstrap.bundle.min.js" ></script>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#fe930a" fill-opacity="1" d="M0,0L60,32C120,64,240,128,360,149.3C480,171,600,149,720,149.3C840,149,960,171,1080,192C1200,213,1320,235,1380,245.3L1440,256L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"></path></svg>

</body>


<footer>
    <body>
        <div class="container-fluid">
            <div class="row p-5 pb-1 bg-dark">
                <div class="col-xs-12 col-md-6 col-lg-3">

                    <div>
                        <a class="h3 text-white text-decoration-none" href="http://www.iaes.edu.ar/">
                            IAES
                        </a>
                    </div>

                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <p class="h5 text-light">Sobre nosotros</p>
                    <div class="mb-3">
                        <a class="text-secondary text-decoration-none" href="http://www.iaes.edu.ar/sobre/">Más informacion</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <p class="h5 text-light">Ayuda</p>
                    <div>
                    <a href="http://www.iaes.edu.ar/contacto/" class="text-secondary text-decoration-none">
                        Acceder
                    </a>
                    </div>
                    <div>
                    <a href="https://goo.gl/maps/DMiLRVpcBgXeGpzK9" class="text-secondary text-decoration-none">
                        Ubicacion
                    </a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <p class="h5 text-white">Contacto</p>
                <div>
                    <a href="https://www.facebook.com/iaespuertorico" class=" text-secondary text-decoration-none" >
                        Facebook
                    </a>
                </div>
                <div>
                    <a href="https://api.whatsapp.com/send/?phone=543743555338&text&type=phone_number&app_absent=0" class=" text-secondary text-decoration-none" >
                        Whatsapp
                    </a>
                </div>
                <div>
                    <a href="#" class=" text-secondary text-decoration-none" >
                        Instagram
                    </a>
                </div>
                </div>
                <div class="col-xs-12">
                    <p class="text-white text-center">Copyright -All rights reserved © 2022 </p>
                </div>
            </div>
        </div>
    </body>
  </footer>


</html>
