<?php
require "../controladores/sessionUser.php";
include("../controladores/conexiondb.php");

$id=$_GET['id'];
$sql = "SELECT * FROM DatosAlumno WHERE id_usuario = '$id'";
$resultado = $conexion->query($sql);
$row = $resultado->fetch_object();

    function obtener_edad_segun_fecha($fecha_nacimiento){
        $nacimiento = new DateTime($fecha_nacimiento);
        $ahora = new DateTime(date("Y-m-d"));
        $diferencia = $ahora->diff($nacimiento);
        return $diferencia->format("%y");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <!-- CSS only -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="estilo.css">
</head>

<body>

<!--nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">IAES</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Inicio</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Filtros de búsqueda
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="busqueda.php">Filtros</a></li>
            <li><a class="dropdown-item" href="#">Ayuda</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Sobre Nosotros</a></li>
          </ul>
      </ul>
    </div>
  </div>
</nav-->
    <?php
    require "navbar.php";
    ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 py-1 px-2">
                <!--<h1 style="text-align:auto;">Perfil</h1>-->
                <h1 class="text-center">
                <?php echo $row->nombre;?></h1>
                <div class="imagen">

                    <img src="../imagenes/mujer.jpg"  style="border-radius: 50%; width:80%; padding-top:10px" alt="...">
                </div>
                <!--<div class="d-grid gap-2 py-4">
                    <button class="btn btn-primary" type="button">Modificar perfil</button>
                    <button class="btn btn-primary" type="button">Busqueda</button>
                </div>
                <div>
                  <a href="#" class="btn btn-primary position-relative">
                  <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                        99+
                        <span class="visually-hidden">unread messages</span>
                      </span>
                  <i class="bi bi-bell"></i>
                  </a>
                </div>-->
                <a class="btn btn-dark position-relative " data-bs-toggle="offcanvas" href="#offcanvasExample" role="button" aria-controls="offcanvasExample">
                <i class="bi bi-gear"></i>
                </a>

                <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
                  <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasExampleLabel">Menú de personalizacion</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                  </div>
                  <div class="offcanvas-body">
                    <div>
                      Por favor, en el siguiente menú seleccione lo que desea editar del perfil de usuario para poder continuar.
                    </div>
                    <div class="dropdown mt-3">
                      <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown">
                        Opciones
                      </button>
                      <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="edicion.php">Subir Curriculum</a></li>
                        <li><a class="dropdown-item" href="#">Editar foto de perfil</a></li>
                        <li><a class="dropdown-item" href="CargaDeDatosPersonales.php">Cambiar datos del Usuario</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <!--<div style="text-align:right">
                  <a href="edicion.php" class="btn btn-dark position-relative">
                    <i class="bi bi-pencil-square"></i>
                  </a>
                  <a href="#" class="btn btn-dark position-relative">
                  <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                        99+
                        <span class="visually-hidden">unread messages</span>
                      </span>
                  <i class="bi bi-bell"></i>
                  </a>
                </div>-->

            </div>
            <div class="col-sm-12 col-md-10 col-lg-10 col-xl-10 py-4 px-10">
            <h1 style="text-align:center">Datos de usuario</h1>

            <p class="text-center">
              <h5>Nombre: <?php echo $row->nombre;?></h5>
              <h5>Apellido: <?php echo $row->apellido;?></h5>
              <h5>Nacionalidad: <?php echo $row->pais;?></h5>
              <h5>Localidad: <?php echo $row->localidad;?></h5>
              <h5>Edad: <?php $fecha = $row->fecha_nacimiento;
                              //echo $fecha;
                              $fecha1 = obtener_edad_segun_fecha($fecha);
                              echo $fecha1;
              ?></h5>


                <button class="btn btn-dark" type="button" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                  Formación Académica
                </button>
                <button class="btn btn-dark" type="button" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="false" aria-controls="collapseWidthExample">
                  Otros Datos
                </button>
                <button class="btn btn-dark" type="button" data-bs-toggle="collapse" data-bs-target="#collapseWidthExample" aria-expanded="false" aria-controls="collapseWidthExample">
                  Ver curriculum
                </button>
              </p>
              <div style="min-height: 100vh;">
                <div class="collapse collapse-horizontal" id="collapse1">
                  <div class="card card-body" style="width: 100vh; ">
                  <p>L</p>
                  </div>
                </div>
                <div class="collapse collapse-horizontal" id="collapse2">
                  <div class="card card-body" style="width: 100vh; ">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dicta, non vero. Itaque dolore, alias totam id voluptatum quo quisquam corrupti, eveniet eius doloribus, nam fugiat nisi ipsam inventore facere necessitatibus!
                  </div>
                </div>

              </div>
        </div>
    </div>

    </div>

    </div>

  <!--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#FE930AC9" fill-opacity="1" d="M0,128L288,288L576,64L864,224L1152,288L1440,0L1440,320L1152,320L864,320L576,320L288,320L0,320Z"></path></svg>-->
  <!--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#fe930a" fill-opacity="1" d="M0,0L60,32C120,64,240,128,360,149.3C480,171,600,149,720,149.3C840,149,960,171,1080,192C1200,213,1320,235,1380,245.3L1440,256L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"></path></svg>-->
  <!--?xml version="1.0" standalone="no"?><svg xmlns="include/wave(1).html" viewBox="0 0 1440 320"><path fill="#fe930a" fill-opacity="1" d="M0,192L48,202.7C96,213,192,235,288,229.3C384,224,480,192,576,181.3C672,171,768,181,864,165.3C960,149,1056,107,1152,101.3C1248,96,1344,128,1392,144L1440,160L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path></svg>
    <!- JavaScript Bundle with Popper -->

    <script src="../bootstrap/js/bootstrap.bundle.min.js" ></script>
</body>

</html>
