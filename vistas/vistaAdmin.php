<!DOCTYPE html>
<html>


  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vista Admin</title>
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!--link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous"-->

    <link href="../estilos/vistaAdmin.css" rel="stylesheet">
  </head>


  <body> 
    <!--nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      

      <div class="container-fluid">
        <a class="navbar-brand" href="index.html" > iAes </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#">Inicio</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Ver más
              </a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#">Filtros</a></li>
                <li><a class="dropdown-item" href="#">Ayuda</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="#">Sobre Nosotros</a></li>
              </ul>
          </ul>
          <form class="d-flex" role="search">
            <input class="form-control me-2" type="search" placeholder="Busqueda por nombre" aria-label="Search">
            <button class="btn btn-outline-secondary" type="submit">Buscar</button>
          </form>
        </div>
      </div>
    </nav-->

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">IAES</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#">Inicio</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Filtros de búsqueda
              </a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#">Filtros</a></li>
                <li><a class="dropdown-item" href="#">Ayuda</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="#">Sobre Nosotros</a></li>
              </ul> 
          </ul>
          <form class="d-flex" role="search">
            <input class="form-control me-2" type="search" placeholder="Busqueda por nombre" aria-label="Search">
            <button class="btn btn-outline-secondary" type="submit">Buscar</button>
          </form>
        </div>
      </div>
    </nav>  
    
    <div class="container">
      <form action="filtro.php" method="POST"> 
        <div class="row justify-content-center mt-3">
          
          <div class="col-sm-12 col-md-6 col-xl-6 col-lg-12">
              <!--div class="imagen">
                <img src="imagenes/logoiaes.png" >
              </div-->
              
              <h1 class="text-center">Filtros</h1>
              <div> 
                <label class="h6"> Carreras</label>
                <select class="form-select" aria-label="Default select example" id="carreras" name="carreras">
                  <option selected value=""> Seleccionar carrera</option>
                  <option value=1 >analista de sistemas</option>
                  <option value=4>Recursos humanos</option>
                  <option value=2>Administracion de empresas</option>
                  <option value=5>Regimen aduanero</option>
                  <option value=6>Turismo y gestion hotelera</option>
                  <option value="v"></option>
                </select>
              </div>

              <div> 
                <label>Capacidad del egresado?</label>
                <select class="form-select" name="capacidad" id="capacidad">
                  <option value="mañana">Mañana</option>
                </select> 
              </div>

              <div> 
                <label>Disponibilidad horaria</label>
                <select class="form-select" name="horario" id="horario" center="align">
                  <option value="tiempoc">Tiempo completo</option>
                  <optgroup label="Medio tiempo :">
                    <option value="mañana">Mañana</option>
                    <option value="tarde">Tarde</option>
                    <option value="noche">Noche</option>
                  </optgroup>
                </select>
              </div>

              <div> 
                <label>Manejo de automovil</label>
                <select class="form-select" name="manejo" id="manejo" center="align">
                  <option value="">No</option>
                  <option value="si">Si</option>
                </select>
              </div>

              <div> 
                <label>Experiencia laboral</label>
                <select class="form-select" name="experiencia" id="experiencia" center="align">
                <option value="">No</option>    
                <option value="si">Si</option>
                  
                </select>
              </div>

              <div> 
                <label>Localidad</label>
                <select class="form-select" name="localidad" id="localidad" center="align">
                  <option selected value=""> Excluyente</option>
                  <option value="puerto rico">Puerto Rico</option>
                  <option value="capiovi">Capiovi</option>
                  <option value="posadas">Posadas</option>
                </select>
              </div>

              <div> 
                <label>Genero</label>
                <select class="form-select" name="genero" id="genero">
                  <option value="">Excluyente</option>
                  <option value="masculino">Masculino</option>
                  <option value="femenino">Femenino</option>
                  <option value="Otro">Otro</option>
                </select> 
              </div>

              <div> 
                <label>Edad</label>
                <select class="form-select" name="edad" id="edad">
                  <option value="1"> Mayor de 18 hasta 30</option>
                  <option value="2">Mayor de 30 hasta 50</option>
                  <option value="3">Mayor de 50 hasta 65</option>
                  <option value="4">Mayor de 18 hasta 65</option>
                </select> 
              </div>

              <div> 
                <label>Estado civil</label>
                <select class="form-select" name="estadoCivil" id="estadoCivil">
                  <option value="">Excluyente</option>
                  <option value="Casado">Casado/a</option>
                  <option value="Soltero">Soltero/a</option>
                  <option value="Divorciado">Divorciado</option>
                  <option value="Viudo">Viudo</option>
                </select> 
              </div>
              <div class="boton1" style="text-align:center;">
                <input type="submit" class="boton"  value="Cargar Datos">
              </div>
              
               
          </div>
          
        </div>
        <br>
        
        
      </form>
    </div>
    
    <!--div class="container-fluid">
      <table>
        <tbody>
          <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Edad</th>
            <th>Genero</th>
            <th>Carrera</th>
            <th>Localidad</th>
          </tr>
          <tr>
            <td>Ana</td>
            <td>Perez</td>
            <td>23</td>
            <td>Femenino</td>
            <td>Recursos humanos</td>
            <td>Capiovi</td>
          </tr>
          <tr>
            <td>Lucas</td>
            <td>Lopez</td>
            <td>27</td>
            <td>Masculino</td>
            <td>Analista de sistemas</td>
            <td>Posadas</td>
          </tr>
        </tbody>
      </table>
    </div-->
    
    
    <!--<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#FE930AC9" fill-opacity="1" d="M0,128L288,288L576,64L864,224L1152,288L1440,0L1440,320L1152,320L864,320L576,320L288,320L0,320Z"></path></svg>-->
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#fe930a" fill-opacity="1" d="M0,0L60,32C120,64,240,128,360,149.3C480,171,600,149,720,149.3C840,149,960,171,1080,192C1200,213,1320,235,1380,245.3L1440,256L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"></path></svg>
    <!-- JavaScript Bundle with Popper -->
    
    
    <script src="../bootstrap/js/bootstrap.bundle.min.js" ></script>
    
    <!--script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script-->
    
  </body>
  <footer>
    <body>
        <div class="container-fluid">
            <div class="row p-5 pb-1 bg-dark ">
                <div class="col-xs-12 col-md-6 col-lg-3">
                    
                    <div>
                        <a class="h3 text-white text-decoration-none" href="http://www.iaes.edu.ar/">
                            IAES
                        </a>
                    </div>
                    
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <p class="h5 text-light">Sobre nosotros</p>
                    <div class="mb-3">
                        <a class="text-secondary text-decoration-none" href="http://www.iaes.edu.ar/sobre/">Más informacion</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <p class="h5 text-light">Ayuda</p>
                    <div>
                    <a href="http://www.iaes.edu.ar/contacto/" class="text-secondary text-decoration-none">
                        Acceder
                    </a>
                    </div>
                    <div>
                    <a href="https://goo.gl/maps/DMiLRVpcBgXeGpzK9" class="text-secondary text-decoration-none">
                        Ubicacion
                    </a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <p class="h5 text-white">Contacto</p>
                <div>
                    <a href="https://www.facebook.com/iaespuertorico" class=" text-secondary text-decoration-none" >
                        Facebook
                    </a>
                </div>
                <div>
                    <a href="https://api.whatsapp.com/send/?phone=543743555338&text&type=phone_number&app_absent=0" class=" text-secondary text-decoration-none" >
                        Whatsapp
                    </a>
                </div>
                <div>
                    <a href="#" class=" text-secondary text-decoration-none" >
                        Instagram
                    </a>
                </div>
                </div>
                <div class="col-xs-12">
                    <p class="text-white text-center">Copyright -All rights reserved © 2022 </p>
                </div>
            </div>
        </div>
    </body>
  </footer>


</html>
