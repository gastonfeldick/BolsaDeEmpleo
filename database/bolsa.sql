-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-11-2022 a las 17:43:52
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bolsa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera`
--

CREATE TABLE `carrera` (
  `id_carrera` int(11) NOT NULL,
  `nombre_carrera` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carrera`
--

INSERT INTO `carrera` (`id_carrera`, `nombre_carrera`) VALUES
(1, 'Técnico Superior Analista de Sistemas de Computación'),
(2, 'Técnico Superior en Administración de Empresas'),
(3, 'Técnico Superior en Gastronomía'),
(4, 'Técnico Superior en Gestión de Recursos Humanos'),
(5, 'Técnico Superior en Régimen Aduanero'),
(6, 'Técnico Superior en Turismo y Gestión Hotelera'),
(7, 'Técnico Superior y Soporte Técnico de Computadoras y Redes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conocimientoinformatico`
--

CREATE TABLE `conocimientoinformatico` (
  `id_alumno` int(11) NOT NULL,
  `id_programas` int(11) NOT NULL,
  `nombre_programa` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactoalumno`
--

CREATE TABLE `contactoalumno` (
  `id_alumno` int(11) NOT NULL,
  `num_celular` varchar(20) NOT NULL,
  `num_fijo` varchar(15) DEFAULT NULL,
  `correo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datosalumno`
--

CREATE TABLE `datosalumno` (
  `id_alumno` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `id_documento` int(11) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `nombre_genero` set('Femenino','Masculino','Otro') NOT NULL,
  `nombre_estado_civil` set('Soltero','Casado','Divorciado','Viudo') NOT NULL,
  `cantidad_hijos` set('0','1','2','3','4','5','6','Más de 6') NOT NULL,
  `nacionalidad` varchar(20) NOT NULL,
  `discapacidad` char(5) NOT NULL,
  `foto` blob DEFAULT NULL,
  `id_localidad` int(11) NOT NULL,
  `id_provincia` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `grupo_sanguineo` set('A negativo','A positivo','B negativo','B positivo','O negativo','O positivo','AB negativo','AB positivo') NOT NULL,
  `id_carrera` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `datosalumno`
--

INSERT INTO `datosalumno` (`id_alumno`, `nombre`, `apellido`, `id_documento`, `fecha_nacimiento`, `nombre_genero`, `nombre_estado_civil`, `cantidad_hijos`, `nacionalidad`, `discapacidad`, `foto`, `id_localidad`, `id_provincia`, `id_pais`, `grupo_sanguineo`, `id_carrera`) VALUES
(0, '', '', 0, '0000-00-00', '', '', '', '', '', NULL, 0, 0, 0, '', 0),
(2, '', '', 0, '0000-00-00', '', '', '', '', '', NULL, 0, 0, 0, '', 0),
(7, '', '', 0, '0000-00-00', '', '', '', '', '', NULL, 0, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disponibilidadhoraria`
--

CREATE TABLE `disponibilidadhoraria` (
  `id_horario` int(11) NOT NULL,
  `tiempo` varchar(20) NOT NULL,
  `turno` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `disponibilidadhoraria`
--

INSERT INTO `disponibilidadhoraria` (`id_horario`, `tiempo`, `turno`) VALUES
(1, 'Tiempo Completo', 'Mañana/Tarde'),
(2, 'Medio Tiempo', 'Mañana'),
(3, 'Medio Tiempo', 'Tarde'),
(4, 'No Disponible', 'No Disponible');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentoidentidad`
--

CREATE TABLE `documentoidentidad` (
  `id_documento` int(11) NOT NULL,
  `tipo_documento` set('Documento Nacional de Identidad','Cédula de Identidad','Cédula de Ciudadania','Documento Personal de Identificación','Documento Único de Identidad') NOT NULL,
  `numero_documento` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id_empresa` int(11) NOT NULL,
  `nombre_empresa` varchar(50) NOT NULL,
  `CUIT` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresacentral`
--

CREATE TABLE `empresacentral` (
  `id_central` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `nombre_dueño` varchar(20) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `encargado` varchar(20) NOT NULL,
  `id_localidad` int(11) NOT NULL,
  `id_provincia` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresasucursal`
--

CREATE TABLE `empresasucursal` (
  `id_sucursal` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `encargado` varchar(20) NOT NULL,
  `id_localidad` int(11) NOT NULL,
  `id_provincia` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `experiencialaboral`
--

CREATE TABLE `experiencialaboral` (
  `id_alumno` int(11) NOT NULL,
  `puesto` varchar(20) NOT NULL,
  `lugar_trabajo` varchar(50) NOT NULL,
  `tarea_desarrollada` text NOT NULL,
  `desde` date NOT NULL,
  `hasta` date NOT NULL,
  `tel_contacto` varchar(15) DEFAULT NULL,
  `nota_recomendacion` longblob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idioma`
--

CREATE TABLE `idioma` (
  `id_alumno` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `lengua` varchar(20) NOT NULL,
  `nivel` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `licencia`
--

CREATE TABLE `licencia` (
  `id_alumno` int(10) UNSIGNED NOT NULL,
  `Licencia` set('Si','No') NOT NULL,
  `categoria` set('A1','A2','B1','B2','B3','C1','C2','C3') DEFAULT NULL,
  `vehiculo` set('Si','No') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidad`
--

CREATE TABLE `localidad` (
  `id_localidad` int(11) NOT NULL,
  `codigo_postal` smallint(6) NOT NULL,
  `nombre_localidad` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id_pais` int(11) NOT NULL,
  `nombre_pais` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id_pais`, `nombre_pais`) VALUES
(1, 'Australia'),
(2, 'Austria'),
(3, 'Azerbaiyán'),
(4, 'Anguilla'),
(5, 'Argentina'),
(6, 'Armenia'),
(7, 'Bielorrusia'),
(8, 'Belice'),
(9, 'Bélgica'),
(10, 'Bermudas'),
(11, 'Bulgaria'),
(12, 'Brasil'),
(13, 'Reino Unido'),
(14, 'Hungría'),
(15, 'Vietnam'),
(16, 'Haiti'),
(17, 'Guadalupe'),
(18, 'Alemania'),
(19, 'Países Bajos, Holanda'),
(20, 'Grecia'),
(21, 'Georgia'),
(22, 'Dinamarca'),
(23, 'Egipto'),
(24, 'Israel'),
(25, 'India'),
(26, 'Irán'),
(27, 'Irlanda'),
(28, 'España'),
(29, 'Italia'),
(30, 'Kazajstán'),
(31, 'Camerún'),
(32, 'Canadá'),
(33, 'Chipre'),
(34, 'Kirguistán'),
(35, 'China'),
(36, 'Costa Rica'),
(37, 'Kuwait'),
(38, 'Letonia'),
(39, 'Libia'),
(40, 'Lituania'),
(41, 'Luxemburgo'),
(42, 'México'),
(43, 'Moldavia'),
(44, 'Mónaco'),
(45, 'Nueva Zelanda'),
(46, 'Noruega'),
(47, 'Polonia'),
(48, 'Portugal'),
(49, 'Reunión'),
(50, 'Rusia'),
(51, 'El Salvador'),
(52, 'Eslovaquia'),
(53, 'Eslovenia'),
(54, 'Surinam'),
(55, 'Estados Unidos'),
(56, 'Tadjikistan'),
(57, 'Turkmenistan'),
(58, 'Islas Turcas y Caicos'),
(59, 'Turquía'),
(60, 'Uganda'),
(61, 'Uzbekistán'),
(62, 'Ucrania'),
(63, 'Finlandia'),
(64, 'Francia'),
(65, 'República Checa'),
(66, 'Suiza'),
(67, 'Suecia'),
(68, 'Estonia'),
(69, 'Corea del Sur'),
(70, 'Japón'),
(71, 'Croacia'),
(72, 'Rumanía'),
(73, 'Hong Kong'),
(74, 'Indonesia'),
(75, 'Jordania'),
(76, 'Malasia'),
(77, 'Singapur'),
(78, 'Taiwan'),
(79, 'Bosnia y Herzegovina'),
(80, 'Bahamas'),
(81, 'Chile'),
(82, 'Colombia'),
(83, 'Islandia'),
(84, 'Corea del Norte'),
(85, 'Macedonia'),
(86, 'Malta'),
(87, 'Pakistán'),
(88, 'Papúa-Nueva Guinea'),
(89, 'Perú'),
(90, 'Filipinas'),
(91, 'Arabia Saudita'),
(92, 'Tailandia'),
(93, 'Emiratos árabes Unidos'),
(94, 'Groenlandia'),
(95, 'Venezuela'),
(96, 'Zimbabwe'),
(97, 'Kenia'),
(98, 'Algeria'),
(99, 'Líbano'),
(100, 'Botsuana'),
(101, 'Tanzania'),
(102, 'Namibia'),
(103, 'Ecuador'),
(104, 'Marruecos'),
(105, 'Ghana'),
(106, 'Siria'),
(107, 'Nepal'),
(108, 'Mauritania'),
(109, 'Seychelles'),
(110, 'Paraguay'),
(111, 'Uruguay'),
(112, 'Congo (Brazzaville)'),
(113, 'Cuba'),
(114, 'Albania'),
(115, 'Nigeria'),
(116, 'Zambia'),
(117, 'Mozambique'),
(119, 'Angola'),
(120, 'Sri Lanka'),
(121, 'Etiopía'),
(122, 'Túnez'),
(123, 'Bolivia'),
(124, 'Panamá'),
(125, 'Malawi'),
(126, 'Liechtenstein'),
(127, 'Bahrein'),
(128, 'Barbados'),
(130, 'Chad'),
(131, 'Man, Isla de'),
(132, 'Jamaica'),
(133, 'Malí'),
(134, 'Madagascar'),
(135, 'Senegal'),
(136, 'Togo'),
(137, 'Honduras'),
(138, 'República Dominicana'),
(139, 'Mongolia'),
(140, 'Irak'),
(141, 'Sudáfrica'),
(142, 'Aruba'),
(143, 'Gibraltar'),
(144, 'Afganistán'),
(145, 'Andorra'),
(147, 'Antigua y Barbuda'),
(149, 'Bangladesh'),
(151, 'Benín'),
(152, 'Bután'),
(154, 'Islas Virgenes Británicas'),
(155, 'Brunéi'),
(156, 'Burkina Faso'),
(157, 'Burundi'),
(158, 'Camboya'),
(159, 'Cabo Verde'),
(164, 'Comores'),
(165, 'Congo (Kinshasa)'),
(166, 'Cook, Islas'),
(168, 'Costa de Marfil'),
(169, 'Djibouti, Yibuti'),
(171, 'Timor Oriental'),
(172, 'Guinea Ecuatorial'),
(173, 'Eritrea'),
(175, 'Feroe, Islas'),
(176, 'Fiyi'),
(178, 'Polinesia Francesa'),
(180, 'Gabón'),
(181, 'Gambia'),
(184, 'Granada'),
(185, 'Guatemala'),
(186, 'Guernsey'),
(187, 'Guinea'),
(188, 'Guinea-Bissau'),
(189, 'Guyana'),
(193, 'Jersey'),
(195, 'Kiribati'),
(196, 'Laos'),
(197, 'Lesotho'),
(198, 'Liberia'),
(200, 'Maldivas'),
(201, 'Martinica'),
(202, 'Mauricio'),
(205, 'Myanmar'),
(206, 'Nauru'),
(207, 'Antillas Holandesas'),
(208, 'Nueva Caledonia'),
(209, 'Nicaragua'),
(210, 'Níger'),
(212, 'Norfolk Island'),
(213, 'Omán'),
(215, 'Isla Pitcairn'),
(216, 'Qatar'),
(217, 'Ruanda'),
(218, 'Santa Elena'),
(219, 'San Cristobal y Nevis'),
(220, 'Santa Lucía'),
(221, 'San Pedro y Miquelón'),
(222, 'San Vincente y Granadinas'),
(223, 'Samoa'),
(224, 'San Marino'),
(225, 'San Tomé y Príncipe'),
(226, 'Serbia y Montenegro'),
(227, 'Sierra Leona'),
(228, 'Islas Salomón'),
(229, 'Somalia'),
(232, 'Sudán'),
(234, 'Swazilandia'),
(235, 'Tokelau'),
(236, 'Tonga'),
(237, 'Trinidad y Tobago'),
(239, 'Tuvalu'),
(240, 'Vanuatu'),
(241, 'Wallis y Futuna'),
(242, 'Sáhara Occidental'),
(243, 'Yemen'),
(246, 'Puerto Rico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferenciatrabajo`
--

CREATE TABLE `preferenciatrabajo` (
  `id_alumno` int(11) NOT NULL,
  `situacion` set('Disponible','No Disponible','No Disponible y En Busqueda') NOT NULL,
  `id_horario` int(11) NOT NULL,
  `puesto_deseado` varchar(30) NOT NULL,
  `cambio_recidencia` varchar(5) NOT NULL,
  `curriculum` longblob NOT NULL,
  `presentacion` blob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `id_provincia` int(11) NOT NULL,
  `nombre_provincia` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redsocial`
--

CREATE TABLE `redsocial` (
  `id_alumno` int(11) NOT NULL,
  `red_social` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secundaria`
--

CREATE TABLE `secundaria` (
  `id_alumno` int(11) NOT NULL,
  `instituto` varchar(50) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `desde` int(11) NOT NULL,
  `hasta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `universidad`
--

CREATE TABLE `universidad` (
  `id_alumno` int(11) NOT NULL,
  `universidad` varchar(50) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `desde` int(11) NOT NULL,
  `hasta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_alumno` int(11) NOT NULL,
  `usuario` varchar(40) NOT NULL,
  `contraseña` varchar(10) NOT NULL,
  `tipo_usuario` set('admin','alumno','empresa') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_alumno`, `usuario`, `contraseña`, `tipo_usuario`) VALUES
(1, 'admin@gmail.com', 'admin', 'admin'),
(2, 'alumno@gmail.com', '2304', 'alumno'),
(3, 'admin2@gmail.com', 'admin', 'admin'),
(4, 'gastonfeldick@hotmai', '2304', 'alumno'),
(5, 'tonchimusic@gmail.co', '2304', 'alumno'),
(6, 'tonchimusic@gmail.co', '2304', 'alumno'),
(7, 'tonchimusic@gmail.com', 'gaston', 'alumno');

INSERT INTO `provincia`(`id_provincia`,`nombre_provincia`) VALUES
(1,'Buenos Aires'),
(2,'Ciudad Autonoma de Buenos'),
(3,'Catamarca'),
(4,'Chaco'),
(5,'Chubut'),
(6,'Córdoba'),
(7,'Corrientes'),
(8,'Entre Ríos'),
(9,'Formosa'),
(10,'Jujuy'),
(11,'La Pampa'),
(12,'La Rioja'),
(13,'Mendoza'),
(14,'Misiones'),
(15,'Neuquén'),
(16,'Río Negro'),
(17,'Salta'),
(18,'San Juan'),
(19,'San Luis'),
(20,'Santa Cruz'),
(21,'Santa Fe'),
(22,'Santiago del Estero'),
(23,'Tierra del fuego'),
(24,'Tucuman');


--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carrera`
--
ALTER TABLE `carrera`
  ADD PRIMARY KEY (`id_carrera`);

--
-- Indices de la tabla `conocimientoinformatico`
--
ALTER TABLE `conocimientoinformatico`
  ADD PRIMARY KEY (`id_programas`,`id_alumno`);

--
-- Indices de la tabla `contactoalumno`
--
ALTER TABLE `contactoalumno`
  ADD PRIMARY KEY (`id_alumno`);

--
-- Indices de la tabla `datosalumno`
--
ALTER TABLE `datosalumno`
  ADD PRIMARY KEY (`id_alumno`);

--
-- Indices de la tabla `disponibilidadhoraria`
--
ALTER TABLE `disponibilidadhoraria`
  ADD PRIMARY KEY (`id_horario`);

--
-- Indices de la tabla `documentoidentidad`
--
ALTER TABLE `documentoidentidad`
  ADD PRIMARY KEY (`id_documento`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id_empresa`);

--
-- Indices de la tabla `empresacentral`
--
ALTER TABLE `empresacentral`
  ADD PRIMARY KEY (`id_central`,`id_empresa`);

--
-- Indices de la tabla `empresasucursal`
--
ALTER TABLE `empresasucursal`
  ADD PRIMARY KEY (`id_sucursal`);

--
-- Indices de la tabla `experiencialaboral`
--
ALTER TABLE `experiencialaboral`
  ADD PRIMARY KEY (`id_alumno`,`lugar_trabajo`);

--
-- Indices de la tabla `idioma`
--
ALTER TABLE `idioma`
  ADD PRIMARY KEY (`id_idioma`,`id_alumno`);

--
-- Indices de la tabla `licencia`
--
ALTER TABLE `licencia`
  ADD PRIMARY KEY (`id_alumno`);

--
-- Indices de la tabla `localidad`
--
ALTER TABLE `localidad`
  ADD PRIMARY KEY (`id_localidad`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `preferenciatrabajo`
--
ALTER TABLE `preferenciatrabajo`
  ADD PRIMARY KEY (`id_alumno`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id_provincia`);

--
-- Indices de la tabla `redsocial`
--
ALTER TABLE `redsocial`
  ADD PRIMARY KEY (`id_alumno`);

--
-- Indices de la tabla `secundaria`
--
ALTER TABLE `secundaria`
  ADD PRIMARY KEY (`id_alumno`);

--
-- Indices de la tabla `universidad`
--
ALTER TABLE `universidad`
  ADD PRIMARY KEY (`id_alumno`,`universidad`,`titulo`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_alumno`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carrera`
--
ALTER TABLE `carrera`
  MODIFY `id_carrera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `conocimientoinformatico`
--
ALTER TABLE `conocimientoinformatico`
  MODIFY `id_programas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `disponibilidadhoraria`
--
ALTER TABLE `disponibilidadhoraria`
  MODIFY `id_horario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `documentoidentidad`
--
ALTER TABLE `documentoidentidad`
  MODIFY `id_documento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id_empresa` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `empresacentral`
--
ALTER TABLE `empresacentral`
  MODIFY `id_central` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `empresasucursal`
--
ALTER TABLE `empresasucursal`
  MODIFY `id_sucursal` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `idioma`
--
ALTER TABLE `idioma`
  MODIFY `id_idioma` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `localidad`
--
ALTER TABLE `localidad`
  MODIFY `id_localidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT de la tabla `provincia`
--
ALTER TABLE `provincia`
  MODIFY `id_provincia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_alumno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
