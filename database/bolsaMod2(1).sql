-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 07-02-2023 a las 23:35:32
-- Versión del servidor: 8.0.32-0ubuntu0.22.04.2
-- Versión de PHP: 8.1.2-1ubuntu2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bolsaMod2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Carrera`
--

CREATE TABLE `Carrera` (
  `id_carrera` int NOT NULL,
  `carrera` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `Carrera`
--

INSERT INTO `Carrera` (`id_carrera`, `carrera`) VALUES
(1, 'Técnico Superior Analista de Sistemas de Computación'),
(2, 'Técnico Superior en Administración de Empresas'),
(3, 'Técnico Superior en Gastronomía'),
(4, 'Técnico Superior en Gestión de Recursos Humanos'),
(5, 'Técnico Superior en Régimen Aduanero'),
(6, 'Técnico Superior en Turismo y Gestión Hotelera'),
(7, 'Técnico Superior y Soporte Técnico de Computadoras y Redes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CarreraAlumno`
--

CREATE TABLE `CarreraAlumno` (
  `id_alumno` int NOT NULL,
  `id_carrera` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ConocimientoInformatico`
--

CREATE TABLE `ConocimientoInformatico` (
  `id_usuario` int NOT NULL,
  `programa` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ContactoAlumno`
--

CREATE TABLE `ContactoAlumno` (
  `id_usuario` int NOT NULL,
  `num_celular` varchar(20) NOT NULL,
  `num_fijo` varchar(15) DEFAULT NULL,
  `correo` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DatosAlumno`
--

CREATE TABLE `DatosAlumno` (
  `id_usuario` int UNSIGNED NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `documento` varchar(30) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `genero` set('Femenino','Masculino','Otro') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `estado_civil` set('Soltero','Casado','Divorciado','Viudo') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `cantidad_hijos` set('0','1','2','3','4','5','6','Más de 6') NOT NULL,
  `nacionalidad` varchar(20) NOT NULL,
  `discapacidad` tinyint(1) NOT NULL,
  `grupo_sanguineo` set('A negativo','A positivo','B negativo','B positivo','O negativo','O positivo','AB negativo','AB positivo') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `localidad` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `codigo_postal` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `id_provincia` int NOT NULL,
  `pais` varchar(90) NOT NULL,
  `foto` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `curriculum` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `DatosAlumno`
--

INSERT INTO `DatosAlumno` (`id_usuario`, `nombre`, `apellido`, `documento`, `fecha_nacimiento`, `genero`, `estado_civil`, `cantidad_hijos`, `nacionalidad`, `discapacidad`, `grupo_sanguineo`, `localidad`, `codigo_postal`, `id_provincia`, `pais`, `foto`, `curriculum`) VALUES
(2, 'Gastón Nicolás', 'Feldick', '41114073', '1998-04-23', 'Masculino', 'Soltero', '0', 'Argentina', 0, 'B negativo', 'Capiovi', '3332', 14, 'Argentina', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DisponibilidadHoraria`
--

CREATE TABLE `DisponibilidadHoraria` (
  `id_horario` int NOT NULL,
  `tiempo` varchar(20) NOT NULL,
  `turno` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `DisponibilidadHoraria`
--

INSERT INTO `DisponibilidadHoraria` (`id_horario`, `tiempo`, `turno`) VALUES
(1, 'Tiempo Completo', 'Mañana/Tarde'),
(2, 'Medio Tiempo', 'Mañana'),
(3, 'Medio Tiempo', 'Tarde'),
(4, 'No Disponible', 'No Disponible');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Empresa`
--

CREATE TABLE `Empresa` (
  `id_usuario` int NOT NULL,
  `empresa` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CUIT` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `EmpresaCentral`
--

CREATE TABLE `EmpresaCentral` (
  `id_usuario` int NOT NULL,
  `dueño` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `encargado` varchar(20) NOT NULL,
  `id_localidad` int NOT NULL,
  `id_provincia` int NOT NULL,
  `id_pais` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `EmpresaSucursal`
--

CREATE TABLE `EmpresaSucursal` (
  `id_sucursal` int NOT NULL,
  `id_usuario` int NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `encargado` varchar(20) NOT NULL,
  `id_localidad` int NOT NULL,
  `id_provincia` int NOT NULL,
  `id_pais` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ExperienciaLaboral`
--

CREATE TABLE `ExperienciaLaboral` (
  `id_usuario` int NOT NULL,
  `puesto` varchar(20) NOT NULL,
  `lugar_trabajo` varchar(50) NOT NULL,
  `tarea_desarrollada` text NOT NULL,
  `desde` date NOT NULL,
  `hasta` date NOT NULL,
  `tel_contacto` varchar(15) DEFAULT NULL,
  `nota_recomendacion` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Idioma`
--

CREATE TABLE `Idioma` (
  `id_usuario` int NOT NULL,
  `idioma` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `nivel` set('A0: Principiante','A1-A2: Básico','A2-B1: Pre-intermedio','B1: Intermedio','B2: Intermedio-Alto','C1-C2: Avanzado') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Licencia`
--

CREATE TABLE `Licencia` (
  `id_usuario` int UNSIGNED NOT NULL,
  `licencia` set('Si','No') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `categoria` set('A1','A2','B1','B2','B3','C1','C2','C3') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `vehiculo` set('Si','No') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Pais`
--

CREATE TABLE `Pais` (
  `id_pais` int NOT NULL,
  `pais` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `Pais`
--

INSERT INTO `Pais` (`id_pais`, `pais`) VALUES
(1, 'Australia'),
(2, 'Austria'),
(3, 'Azerbaiyán'),
(4, 'Anguilla'),
(5, 'Argentina'),
(6, 'Armenia'),
(7, 'Bielorrusia'),
(8, 'Belice'),
(9, 'Bélgica'),
(10, 'Bermudas'),
(11, 'Bulgaria'),
(12, 'Brasil'),
(13, 'Reino Unido'),
(14, 'Hungría'),
(15, 'Vietnam'),
(16, 'Haiti'),
(17, 'Guadalupe'),
(18, 'Alemania'),
(19, 'Países Bajos, Holanda'),
(20, 'Grecia'),
(21, 'Georgia'),
(22, 'Dinamarca'),
(23, 'Egipto'),
(24, 'Israel'),
(25, 'India'),
(26, 'Irán'),
(27, 'Irlanda'),
(28, 'España'),
(29, 'Italia'),
(30, 'Kazajstán'),
(31, 'Camerún'),
(32, 'Canadá'),
(33, 'Chipre'),
(34, 'Kirguistán'),
(35, 'China'),
(36, 'Costa Rica'),
(37, 'Kuwait'),
(38, 'Letonia'),
(39, 'Libia'),
(40, 'Lituania'),
(41, 'Luxemburgo'),
(42, 'México'),
(43, 'Moldavia'),
(44, 'Mónaco'),
(45, 'Nueva Zelanda'),
(46, 'Noruega'),
(47, 'Polonia'),
(48, 'Portugal'),
(49, 'Reunión'),
(50, 'Rusia'),
(51, 'El Salvador'),
(52, 'Eslovaquia'),
(53, 'Eslovenia'),
(54, 'Surinam'),
(55, 'Estados Unidos'),
(56, 'Tadjikistan'),
(57, 'Turkmenistan'),
(58, 'Islas Turcas y Caicos'),
(59, 'Turquía'),
(60, 'Uganda'),
(61, 'Uzbekistán'),
(62, 'Ucrania'),
(63, 'Finlandia'),
(64, 'Francia'),
(65, 'República Checa'),
(66, 'Suiza'),
(67, 'Suecia'),
(68, 'Estonia'),
(69, 'Corea del Sur'),
(70, 'Japón'),
(71, 'Croacia'),
(72, 'Rumanía'),
(73, 'Hong Kong'),
(74, 'Indonesia'),
(75, 'Jordania'),
(76, 'Malasia'),
(77, 'Singapur'),
(78, 'Taiwan'),
(79, 'Bosnia y Herzegovina'),
(80, 'Bahamas'),
(81, 'Chile'),
(82, 'Colombia'),
(83, 'Islandia'),
(84, 'Corea del Norte'),
(85, 'Macedonia'),
(86, 'Malta'),
(87, 'Pakistán'),
(88, 'Papúa-Nueva Guinea'),
(89, 'Perú'),
(90, 'Filipinas'),
(91, 'Arabia Saudita'),
(92, 'Tailandia'),
(93, 'Emiratos árabes Unidos'),
(94, 'Groenlandia'),
(95, 'Venezuela'),
(96, 'Zimbabwe'),
(97, 'Kenia'),
(98, 'Algeria'),
(99, 'Líbano'),
(100, 'Botsuana'),
(101, 'Tanzania'),
(102, 'Namibia'),
(103, 'Ecuador'),
(104, 'Marruecos'),
(105, 'Ghana'),
(106, 'Siria'),
(107, 'Nepal'),
(108, 'Mauritania'),
(109, 'Seychelles'),
(110, 'Paraguay'),
(111, 'Uruguay'),
(112, 'Congo (Brazzaville)'),
(113, 'Cuba'),
(114, 'Albania'),
(115, 'Nigeria'),
(116, 'Zambia'),
(117, 'Mozambique'),
(119, 'Angola'),
(120, 'Sri Lanka'),
(121, 'Etiopía'),
(122, 'Túnez'),
(123, 'Bolivia'),
(124, 'Panamá'),
(125, 'Malawi'),
(126, 'Liechtenstein'),
(127, 'Bahrein'),
(128, 'Barbados'),
(130, 'Chad'),
(131, 'Man, Isla de'),
(132, 'Jamaica'),
(133, 'Malí'),
(134, 'Madagascar'),
(135, 'Senegal'),
(136, 'Togo'),
(137, 'Honduras'),
(138, 'República Dominicana'),
(139, 'Mongolia'),
(140, 'Irak'),
(141, 'Sudáfrica'),
(142, 'Aruba'),
(143, 'Gibraltar'),
(144, 'Afganistán'),
(145, 'Andorra'),
(147, 'Antigua y Barbuda'),
(149, 'Bangladesh'),
(151, 'Benín'),
(152, 'Bután'),
(154, 'Islas Virgenes Británicas'),
(155, 'Brunéi'),
(156, 'Burkina Faso'),
(157, 'Burundi'),
(158, 'Camboya'),
(159, 'Cabo Verde'),
(164, 'Comores'),
(165, 'Congo (Kinshasa)'),
(166, 'Cook, Islas'),
(168, 'Costa de Marfil'),
(169, 'Djibouti, Yibuti'),
(171, 'Timor Oriental'),
(172, 'Guinea Ecuatorial'),
(173, 'Eritrea'),
(175, 'Feroe, Islas'),
(176, 'Fiyi'),
(178, 'Polinesia Francesa'),
(180, 'Gabón'),
(181, 'Gambia'),
(184, 'Granada'),
(185, 'Guatemala'),
(186, 'Guernsey'),
(187, 'Guinea'),
(188, 'Guinea-Bissau'),
(189, 'Guyana'),
(193, 'Jersey'),
(195, 'Kiribati'),
(196, 'Laos'),
(197, 'Lesotho'),
(198, 'Liberia'),
(200, 'Maldivas'),
(201, 'Martinica'),
(202, 'Mauricio'),
(205, 'Myanmar'),
(206, 'Nauru'),
(207, 'Antillas Holandesas'),
(208, 'Nueva Caledonia'),
(209, 'Nicaragua'),
(210, 'Níger'),
(212, 'Norfolk Island'),
(213, 'Omán'),
(215, 'Isla Pitcairn'),
(216, 'Qatar'),
(217, 'Ruanda'),
(218, 'Santa Elena'),
(219, 'San Cristobal y Nevis'),
(220, 'Santa Lucía'),
(221, 'San Pedro y Miquelón'),
(222, 'San Vincente y Granadinas'),
(223, 'Samoa'),
(224, 'San Marino'),
(225, 'San Tomé y Príncipe'),
(226, 'Serbia y Montenegro'),
(227, 'Sierra Leona'),
(228, 'Islas Salomón'),
(229, 'Somalia'),
(232, 'Sudán'),
(234, 'Swazilandia'),
(235, 'Tokelau'),
(236, 'Tonga'),
(237, 'Trinidad y Tobago'),
(239, 'Tuvalu'),
(240, 'Vanuatu'),
(241, 'Wallis y Futuna'),
(242, 'Sáhara Occidental'),
(243, 'Yemen'),
(246, 'Puerto Rico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PreferenciaTrabajo`
--

CREATE TABLE `PreferenciaTrabajo` (
  `id_usuario` int NOT NULL,
  `situacion` set('Disponible','No Disponible','No Disponible y En Busqueda') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `id_horario` int NOT NULL,
  `puesto_deseado` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `cambio_recidencia` varchar(5) NOT NULL,
  `presentacion` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Provincia`
--

CREATE TABLE `Provincia` (
  `id_provincia` int NOT NULL,
  `provincia` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `Provincia`
--

INSERT INTO `Provincia` (`id_provincia`, `provincia`) VALUES
(1, 'Buenos Aires'),
(2, 'Ciudad Autonoma de Buenos'),
(3, 'Catamarca'),
(4, 'Chaco'),
(5, 'Chubut'),
(6, 'Córdoba'),
(7, 'Corrientes'),
(8, 'Entre Ríos'),
(9, 'Formosa'),
(10, 'Jujuy'),
(11, 'La Pampa'),
(12, 'La Rioja'),
(13, 'Mendoza'),
(14, 'Misiones'),
(15, 'Neuquén'),
(16, 'Río Negro'),
(17, 'Salta'),
(18, 'San Juan'),
(19, 'San Luis'),
(20, 'Santa Cruz'),
(21, 'Santa Fe'),
(22, 'Santiago del Estero'),
(23, 'Tierra del fuego'),
(24, 'Tucuman');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `RedSocial`
--

CREATE TABLE `RedSocial` (
  `id_usuario` int NOT NULL,
  `red_social` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Secundaria`
--

CREATE TABLE `Secundaria` (
  `id_usuario` int NOT NULL,
  `instituto` varchar(50) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `desde` int NOT NULL,
  `hasta` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Universidad`
--

CREATE TABLE `Universidad` (
  `id_usuario` int NOT NULL,
  `universidad` varchar(50) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `desde` int NOT NULL,
  `hasta` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuario`
--

CREATE TABLE `Usuario` (
  `id_usuario` int NOT NULL,
  `usuario` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `contraseña` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `tipo_usuario` set('admin','alumno','empresa') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `Usuario`
--

INSERT INTO `Usuario` (`id_usuario`, `usuario`, `contraseña`, `tipo_usuario`) VALUES
(1, 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(2, 'alumnos@gmail.com', 'd0dd8771aed880dd8c1f39013fe4eb65', 'alumno');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `CarreraAlumno`
--
ALTER TABLE `CarreraAlumno`
  ADD PRIMARY KEY (`id_alumno`,`id_carrera`);

--
-- Indices de la tabla `ConocimientoInformatico`
--
ALTER TABLE `ConocimientoInformatico`
  ADD PRIMARY KEY (`id_usuario`,`programa`);

--
-- Indices de la tabla `ContactoAlumno`
--
ALTER TABLE `ContactoAlumno`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `DatosAlumno`
--
ALTER TABLE `DatosAlumno`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `DisponibilidadHoraria`
--
ALTER TABLE `DisponibilidadHoraria`
  ADD PRIMARY KEY (`id_horario`);

--
-- Indices de la tabla `Empresa`
--
ALTER TABLE `Empresa`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `EmpresaCentral`
--
ALTER TABLE `EmpresaCentral`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `EmpresaSucursal`
--
ALTER TABLE `EmpresaSucursal`
  ADD PRIMARY KEY (`id_sucursal`,`id_usuario`);

--
-- Indices de la tabla `ExperienciaLaboral`
--
ALTER TABLE `ExperienciaLaboral`
  ADD PRIMARY KEY (`id_usuario`,`lugar_trabajo`);

--
-- Indices de la tabla `Idioma`
--
ALTER TABLE `Idioma`
  ADD PRIMARY KEY (`id_usuario`,`idioma`);

--
-- Indices de la tabla `Licencia`
--
ALTER TABLE `Licencia`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `Pais`
--
ALTER TABLE `Pais`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `PreferenciaTrabajo`
--
ALTER TABLE `PreferenciaTrabajo`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `Provincia`
--
ALTER TABLE `Provincia`
  ADD PRIMARY KEY (`id_provincia`);

--
-- Indices de la tabla `RedSocial`
--
ALTER TABLE `RedSocial`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `Secundaria`
--
ALTER TABLE `Secundaria`
  ADD PRIMARY KEY (`id_usuario`,`instituto`);

--
-- Indices de la tabla `Universidad`
--
ALTER TABLE `Universidad`
  ADD PRIMARY KEY (`id_usuario`,`universidad`,`titulo`);

--
-- Indices de la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `DisponibilidadHoraria`
--
ALTER TABLE `DisponibilidadHoraria`
  MODIFY `id_horario` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `EmpresaSucursal`
--
ALTER TABLE `EmpresaSucursal`
  MODIFY `id_sucursal` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `Pais`
--
ALTER TABLE `Pais`
  MODIFY `id_pais` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT de la tabla `Provincia`
--
ALTER TABLE `Provincia`
  MODIFY `id_provincia` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `Usuario`
--
ALTER TABLE `Usuario`
  MODIFY `id_usuario` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
