-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 06-10-2022 a las 21:13:30
-- Versión del servidor: 8.0.30-0ubuntu0.22.04.1
-- Versión de PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `BolsaDeEmpleo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conocimientos_informaticos`
--

CREATE TABLE `conocimientos_informaticos` (
  `id` int NOT NULL,
  `word` varchar(20) NOT NULL,
  `excel` varchar(20) NOT NULL,
  `programa1` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Egresado`
--

CREATE TABLE `Egresado` (
  `dni` varchar(8) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `fecha_nac` date NOT NULL,
  `genero` varchar(10) NOT NULL,
  `estado_civil` varchar(20) NOT NULL,
  `hijos` tinyint(1) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `telefono fijo` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `redesocial` varchar(40) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  `ciudad` varchar(20) NOT NULL,
  `codigo_postal` smallint NOT NULL,
  `provincia` varchar(25) NOT NULL,
  `pais` varchar(30) NOT NULL,
  `nacionalidad` varchar(30) NOT NULL,
  `grupo_sanguineo` int NOT NULL,
  `licencia_conducir` tinyint(1) NOT NULL,
  `vehiculo` tinyint(1) NOT NULL,
  `discapacidad` tinyint(1) NOT NULL,
  `foto` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `experiencia_profesional`
--

CREATE TABLE `experiencia_profesional` (
  `id` int NOT NULL,
  `puesto` int NOT NULL,
  `lugarTrabajo` int NOT NULL,
  `tarea` int NOT NULL,
  `desde` int NOT NULL,
  `hasta` int NOT NULL,
  `telefono_empresa` int NOT NULL,
  `cartarecomendacion` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formacion_academica`
--

CREATE TABLE `formacion_academica` (
  `id` int NOT NULL,
  `universitario` varchar(30) NOT NULL,
  `desdeUni` varchar(5) NOT NULL,
  `hastaUni` varchar(5) NOT NULL,
  `terciario` varchar(30) NOT NULL,
  `desdeTerciario` varchar(5) NOT NULL,
  `hastaTerciario` varchar(5) NOT NULL,
  `secundario` varchar(30) NOT NULL,
  `primario` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habilidades`
--

CREATE TABLE `habilidades` (
  `id` int NOT NULL,
  `habilidad` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas`
--

CREATE TABLE `idiomas` (
  `id` int NOT NULL,
  `idioma1` int NOT NULL,
  `idioma2` int NOT NULL,
  `idioma3` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_profesional`
--

CREATE TABLE `perfil_profesional` (
  `id` int NOT NULL,
  `cargo_titulo` varchar(30) NOT NULL,
  `descripcionperfil` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preferencia_trabajo`
--

CREATE TABLE `preferencia_trabajo` (
  `id` int NOT NULL,
  `situacionactual` varchar(20) NOT NULL,
  `puestodeseado` varchar(20) NOT NULL,
  `disponibiliad` varchar(20) NOT NULL,
  `viajar` tinyint(1) NOT NULL,
  `cambioresidencia` tinyint(1) NOT NULL,
  `curriculum` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Egresado`
--
ALTER TABLE `Egresado`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `experiencia_profesional`
--
ALTER TABLE `experiencia_profesional`
  ADD PRIMARY KEY (`id`,`puesto`);

--
-- Indices de la tabla `formacion_academica`
--
ALTER TABLE `formacion_academica`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `idiomas`
--
ALTER TABLE `idiomas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `preferencia_trabajo`
--
ALTER TABLE `preferencia_trabajo`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
